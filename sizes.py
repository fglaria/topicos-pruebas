import sys
import os

if 2 > len(sys.argv):
    print "Modo de uso: " + sys.argv[0] + " RUTA"
    sys.exit()

# Path to file
path = sys.argv[1]

extensions = (
    "seqX.bin",
    "X-wmint.sdsl",
    "B-rrr-64.sdsl",
    "B4.bin",
    "seqY3.bin",
    "Y3-wmint.sdsl"
)

for extension in extensions:
    statinfo = os.stat(path + '.' + extension)
    print extension + " size " + str(statinfo.st_size*8) + " bits"
