#include "binary_functions.h"

namespace binary
{
    std::string print(unsigned short c, unsigned int y, bool separate) {
        std::stringstream out;

        for (int i = y-1; i >= 0; --i)
        {
            out << ((c & (1 << i))? '1' : '0');

            if (separate && 8 < y && 0 == i%8)
            {
                out << " ";
            }
        }

        return out.str();
    }

}
