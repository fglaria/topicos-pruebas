#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>
#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/wavelet_trees.hpp>

using namespace std;
using namespace sdsl;

/* Time meassuring */
double ticks;
struct tms t1,t2;

void start_clock() {
        times (&t1);
}

double stop_clock() {
        times (&t2);
        return (t2.tms_utime-t1.tms_utime)/ticks;
}
/* end Time meassuring */


class posclass {
   public:
	uint64_t l;
	uint64_t m;
	uint64_t r; uint64_t rs;
  posclass(){l=0; m=0; r=0; rs =0;}
  posclass(uint64_t il, uint64_t im, uint64_t ir, uint64_t size){
	l=il; m=im; r=ir; rs = size;
  }
};


vector<unsigned int> splitToInt(string line, string delims){
   string::size_type bi, ei;
   vector<unsigned int> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
  	ei = line.find_first_of(delims, bi);
    	if(ei == string::npos)
      		ei = line.length();
		string aux = line.substr(bi, ei-bi);
    		words.push_back((unsigned int)atoi(aux.c_str()));
    		bi = line.find_first_not_of(delims, ei);
  	}
  return words;
}

void readBitmapFile(string bfile, vector<uint> &fields){
   string line, word;
   ifstream infile(bfile.c_str());
   while(getline(infile,line)){
        if(line.size() == 0)
                continue;
        fields = splitToInt(line, " ");
   }
   infile.close();
   return;
}

void readRRRBitmap(rrr_vector<63> &rrrb, string indir, string outname ){
  outname = indir + outname + "-rrr-64.sdsl";
  load_from_file(rrrb,outname.c_str());
}

void readSDBitmap(sd_vector<> &sdb, string indir, string outname ){
  outname = indir + outname + "-sdb.sdsl";
  load_from_file(sdb,outname.c_str());
}

void readWISeqs(wt_int<rrr_vector<63> > &wtint, wt_int<rrr_vector<63> > &bcint, string indir, string inname ){
    string wtfile = indir + inname + ".seqX-wtint.sdsl";
    string bcfile = indir + inname + ".seqBC-wtint.sdsl";
    load_from_file(wtint,wtfile.c_str());
    load_from_file(bcint,bcfile.c_str());
}

void readWMSeqs(wm_int<rrr_vector<63> > &wmint, wm_int<rrr_vector<63> > &bcm, string indir, string inname ){
    string wtfile = indir + inname + ".seqX-wmint.sdsl";
    string bcfile = indir + inname + ".seqBC-wmint.sdsl";
    load_from_file(wmint,wtfile.c_str());
    load_from_file(bcm,bcfile.c_str());
}

void computeEdges(map<uint64_t, set<uint64_t> > &adjlist, set<uint64_t> &cliques){
    set<uint64_t>::iterator si, si1;
    map<uint64_t, set<uint64_t> >::iterator it;
    for(si=cliques.begin(); si != cliques.end(); si++){
            set<uint64_t> outlist;
            it = adjlist.find(*si);
            if (it != adjlist.end()){
                outlist = it->second;
            }
    	    for(si1=cliques.begin(); si1 != cliques.end(); si1++){
                if(*si != *si1)
                        outlist.insert(*si1);
            }
            adjlist[*si] = outlist;
    }
}

void computeVEdges(uint64_t v, set<uint64_t> &outlinks, set<uint64_t> &cliques){
    set<uint64_t>::iterator si, si1;
    for(si=cliques.begin(); si != cliques.end(); si++){
    	    for(si1=cliques.begin(); si1 != cliques.end(); si1++){
		if(*si != v && *si1 != v) continue;
                if(*si != *si1)
                        outlinks.insert(*si1);
            }
    }
    outlinks.erase(v);
}

void printSet(set<uint64_t> &s){
  set<uint64_t>::iterator si;
  for(si = s.begin(); si != s.end(); si++)
	cout<<*si<<" ";
  cout<<endl;

}

void printSetPlus1(set<uint64_t> &s){
  set<uint64_t>::iterator si;
  for(si = s.begin(); si != s.end(); si++)
	cout<<*si+1<<" ";
  cout<<endl;

}

void alledgesBiclique(set<uint64_t> &setS, set<uint64_t> &setC, map<uint64_t, set<uint64_t> > &adjlist){
	set<uint64_t>::iterator s,c;
	map<uint64_t, set<uint64_t> >::iterator m;
	for(s=setS.begin(); s!=setS.end(); s++){
		set<uint64_t> outs;
		m = adjlist.find(*s);
		if(m != adjlist.end()){
			outs = m->second;
		}
		for(c=setC.begin(); c != setC.end(); c++){
			outs.insert(*c);
		}
		adjlist[*s] = outs;
	}
}

void computeBiCliquesCluster(int cluster, map< uint64_t, set<uint64_t> > &adjlist, map<uint64_t, pair<posclass, posclass > > &xbcpos, map<uint64_t, posclass> &xsizes, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
	map<uint64_t, pair< set<uint64_t>, set<uint64_t> > > ::iterator it;
	pair<posclass, posclass> startx_bc = xbcpos[cluster];
	posclass startx = startx_bc.first;
	uint64_t nCC = startx.rs;
	posclass startbc = startx_bc.second;
	uint64_t rsize = startbc.rs;
	uint64_t end_rx = startx.r + rsize; // last index in seqX for R component (last not included)
	uint64_t end_rbc = startbc.r + nCC*rsize; // last index in BC of R component (last not included)
	uint64_t stxl = startx.l;
	uint64_t stxm = startx.m;
	uint64_t stxr = startx.r;

 	if(nCC == 1){
		// no need to check bc bitmap, only seqX
		set<uint64_t> setS, setC;
		for(uint64_t i=startx.l; i<startx.m; i++){
			setS.insert(wmint[i]);
		}
		for(uint64_t i=startx.m; i<startx.r; i++){
			setS.insert(wmint[i]);
			setC.insert(wmint[i]);
		}
		for(uint64_t i=startx.r; i<end_rx; i++){
			setC.insert(wmint[i]);
		}
		//cout<<" cluster "<<cluster<<" nCC = 1"<<endl;
		//printSet(setS);
		//printSet(setC);
		alledgesBiclique(setS, setC, adjlist);
		return;
	}


	uint64_t j = 0;
	uint64_t symbol;
	map<uint64_t, pair< set<uint64_t>, set<uint64_t> > > bicliques;
	map<uint64_t, pair< set<uint64_t>, set<uint64_t> > >::iterator bit;

	uint64_t endbcl = startbc.m;
	if(startx.m == startx.r){ // no M component
		endbcl = startbc.r;
	}

	//cout<<"cluster "<<cluster<<" startx.l "<<startx.l<<" startx.m "<<startx.m<<" startx.r "<<startx.r<<" startx.rs "<<startx.rs<<endl;
	//cout<<"cluster "<<cluster<<" startbc.l "<<startbc.l<<" startbc.m "<<startbc.m<<" startbc.r "<<startbc.r<<" startbc.rs "<<startbc.rs<<endl;
	//cout<<"cluster "<<cluster<<" xsizes l "<<xsizes[cluster].l<<" m "<<xsizes[cluster].m<<" r "<<xsizes[cluster].r<<" lastbc "<<xsizes[cluster].rs<<endl;
	//for(uint64_t i=startbc.l; i<startbc.m; i++){
	if(xsizes[cluster].l > 0)
	for(uint64_t i=startbc.l; i<endbcl; i++){
		uint64_t bcval = rrrb_bc[i];
		if (((i-startbc.l) % nCC) == 0) { // starts pos of next seqX symbol
			symbol = wmint[stxl];
			//cout<<" i "<<i<<" symbol "<<symbol<<" bcval "<<bcval<<endl;
			j=0;
			set<uint64_t> setS;
			set<uint64_t> setC;
			if(bcval == 1){
				it = bicliques.find(j);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				//cout<<"L adding symbol "<<symbol<<" to S to biclique "<<j<<endl;
				setS.insert(symbol);
				bicliques[j] = make_pair(setS, setC);
			}
		} else {
			set<uint64_t> setS;
			set<uint64_t> setC;
			if(bcval == 1){
				it = bicliques.find(j);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				setS.insert(symbol);
				bicliques[j] = make_pair(setS, setC);
			}
			if(j == nCC - 1)
				stxl++;
		}
		j++;
	}

	uint64_t nCC2 = 2*nCC;
	int active = 0;
	unsigned int k;
	if(startx.r > startx.m){
	   uint64_t endbcm = xsizes[cluster].m *nCC2 + startbc.m;
	   //for(uint64_t i=startbc.m; i<startbc.r; i++){
	   for(uint64_t i=startbc.m; i<endbcm; i++){
		uint64_t bcval = rrrb_bc[i];
		if (((i-startbc.m) % nCC2) == 0) { // starts pos of next seqX symbol
			j=0;
			active = 1;
		} else if (((i-startbc.m) % nCC ) == 0) { // starts pos of next seqX symbol
			active = 0;
		}
		if (((i-startbc.m) % nCC) == 0) { // starts pos of next seqX symbol
			symbol = wmint[stxm];
			//j=0;
			k=0;
			//set<uint64_t> setS;
			//set<uint64_t> setC;
			if(bcval == 1){
				set<uint64_t> setS;
				set<uint64_t> setC;
				it = bicliques.find(k);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				if (((i-startbc.m) % nCC2) == 0) { // starts pos of next seqX symbol
					//j=0;
					//active = 1;
					setS.insert(symbol);
					//cout<<"k "<<k<<" i-startbc.m "<<i-startbc.m<<" inserting S "<<symbol<<endl;
				} else if (((i-startbc.m) % nCC ) == 0) { // starts pos of next seqX symbol
					//active = 0;
					setC.insert(symbol);
					//cout<<"k "<<k<<" i-startbc.m "<<i-startbc.m<<" inserting C "<<symbol<<endl;
				}
				//printSet(setS);
				//printSet(setC);
				bicliques[k] = make_pair(setS, setC);
			}
		} else {
			//set<uint64_t> setS;
			//set<uint64_t> setC;
			if(bcval == 1){
				set<uint64_t> setS;
				set<uint64_t> setC;
				it = bicliques.find(k);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				if (active) { // starts pos of next seqX symbol
					setS.insert(symbol);
					//cout<<"k "<<k<<" i-startbc.m "<<i-startbc.m<<" inserting S "<<symbol<<endl;
				} else if (active == 0) { // starts pos of next seqX symbol
					setC.insert(symbol);
					//cout<<"k "<<k<<" i-startbc.m "<<i-startbc.m<<" inserting C "<<symbol<<endl;
				}
				//printSet(setS);
				//printSet(setC);
				bicliques[k] = make_pair(setS, setC);
			}
			if(j == nCC2 - 1)
				stxm++;
		}
		j++;
		k++;
	   }
	}

/*
	cout<<" cluster m "<<cluster<<endl;
	for(bit=bicliques.begin(); bit!=bicliques.end(); bit++){
		cout<<"bit "<<bit->first<<endl;
		printSet(bicliques[bit->first].first);
		printSet(bicliques[bit->first].second);
	}
*/

  	if(xsizes[cluster].r > 0)
	for(uint64_t i=startbc.r; i<end_rbc; i++){
		uint64_t bcval = rrrb_bc[i];
		if (((i-startbc.r) % nCC) == 0) { // starts pos of next seqX symbol
			symbol = wmint[stxr];
			j=0;
			set<uint64_t> setS, setC;
			if(bcval == 1){
				it = bicliques.find(j);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				setC.insert(symbol);
				bicliques[j] = make_pair(setS, setC);
			}
		} else {
			set<uint64_t> setS;
			set<uint64_t> setC;
			if(bcval == 1){
				it = bicliques.find(j);
				if( it != bicliques.end()){
					pair< set<uint64_t>, set<uint64_t> > psc = it->second;
					setS = psc.first;
					setC = psc.second;
				}
				setC.insert(symbol);
				bicliques[j] = make_pair(setS, setC);
			}
			if(j == nCC - 1)
				stxr++;
		}
		j++;
	}

/*
	cout<<" cluster r "<<cluster<<endl;
	for(bit=bicliques.begin(); bit!=bicliques.end(); bit++){
		cout<<"R bit "<<bit->first<<endl;
		printSet(bicliques[bit->first].first);
		printSet(bicliques[bit->first].second);
	}
*/

	for(it=bicliques.begin(); it!=bicliques.end(); it++){
		pair<set<uint64_t>, set<uint64_t> > psc = it->second;
		alledgesBiclique(psc.first, psc.second, adjlist);
	}
}

void computeXBcPos(map<uint64_t, pair<posclass, posclass> > &xbcpos, map<uint64_t, posclass> &xsizes, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
    rrr_vector<63>::rank_1_type rrrbB_rank(&rrrb_B);
    rrr_vector<63>::rank_0_type rrrbB_rank0(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbB_sel(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbbc_sel(&rrrb_bc);
    rrr_vector<63>::rank_1_type rrrbbc_rank(&rrrb_bc);
    uint64_t count = rrrbB_rank(rrrb_B.size());
    uint64_t nclusters = count/3;
    uint64_t nCC; // number of cliques in this cluster

    uint64_t prevcsize = 0, clustersize = 0;
    uint64_t bpos, bpos1, imod, lpos, mpos, rpos;
    uint64_t lsize=0, msize=0, rsize=0;
    uint64_t bclpos=0, bcmpos=0, bcrpos=0, prev_nCC=0;

    map<uint64_t, pair<posclass, posclass> >::iterator mm;
    uint64_t cid=1, lastbc = 0;
    //map<uint64_t, posclass> xsizes;


    for(uint64_t i=1; i<count; i++){
       nCC = wmbcint[cid-1]; // number of cliques in this cluster
       //cout<<"i "<<i<<" nCC "<<nCC<<endl;
       bpos = rrrbB_sel(i);
       bpos1 = rrrbB_sel(i+1);
       imod = i%3;
       if(imod == 1){ // one that starts L component
	 lpos = rrrbB_rank0(bpos);
	 lsize = rrrbB_rank0(bpos1) - lpos;
	 if(lsize > 0 && i > 1){ // it has L component
		//cout<<" lsize "<<lsize<<" lpos "<<lpos<<endl;
		posclass prevbc = xbcpos[cid-1].second;
		prev_nCC = xbcpos[cid-1].first.rs;
		posclass prevxpos = xbcpos[cid-1].first;
		bclpos = xsizes[cid-1].rs;
	 } else {
		bclpos = 0;
	 }
       } else if(imod == 2){ // one that starts M component
	 mpos = rrrbB_rank0(bpos);
	 msize = rrrbB_rank0(bpos1) - mpos;
	 if(msize > 0){ // it has M component
		//cout<<" msize "<<msize<<" bclpos "<<bclpos<<" lsize "<<lsize<<" nCC "<<nCC<<endl;
		posclass prevbc = xbcpos[cid-1].second;
		prev_nCC = xbcpos[cid-1].first.rs;
		posclass prevxpos = xbcpos[cid-1].first;
		bcmpos = xsizes[cid-1].rs + lsize*nCC;
	 } else {
		bcmpos = 0;
	 }
       } else if(imod == 0){ // one that starts R component
	 rpos = rrrbB_rank0(bpos);
	 rsize = rrrbB_rank0(bpos1) - rpos;
	 if(rsize > 0){ // it has M component
		//cout<<" rsize "<<rsize<<" rpos "<<rpos<<" msize "<<msize<<" nCC "<<nCC<<endl;
		posclass prevbc = xbcpos[cid-1].second;
		prev_nCC = xbcpos[cid-1].first.rs;
		posclass prevxpos = xbcpos[cid-1].first;
		if(msize > 0 && nCC > 1){
			bcrpos = bcmpos + 2*msize*nCC;
		} else if(msize > 0 && nCC == 1){
			bcrpos = bcmpos + msize;
		} else if(msize == 0 && lsize > 0)
			bcrpos = xsizes[cid-1].rs + lsize*nCC;
			//cout<<" bcrpos "<<bcrpos<<endl;
	 } else {
		//bcrpos = 0;
		bcrpos = bcmpos;
	 }
         clustersize = lsize + msize + rsize; // number of symbols in this cluster
	 uint64_t countMsize = 0;
	 if(nCC == 1)
		countMsize = msize;
	 else if(nCC > 1)
		countMsize = 2*nCC*msize;
	 if(cid > 1)
 	    //lastbc = lsize*nCC + msize*2*nCC + rsize*nCC + xsizes[cid-1].rs;
 	    lastbc = lsize*nCC + countMsize + rsize*nCC + xsizes[cid-1].rs;
	 else
 	    //lastbc = lsize*nCC + msize*2*nCC + rsize*nCC;
 	    lastbc = lsize*nCC + countMsize + rsize*nCC;

	 //cout<<" cid "<<cid<<" lastbc "<<lastbc<<endl;
         posclass compsizes = posclass(lsize, msize, rsize, lastbc);
	 xsizes[cid] = compsizes;
	 //cout<<" lsize "<<lsize<<" msize "<<msize<<" rsize "<<rsize<<endl;
	 posclass cbc = posclass(bclpos, bcmpos, bcrpos, rsize);
	 posclass cx = posclass(lpos, mpos, rpos, nCC);
	 xbcpos[cid] = make_pair(cx, cbc);
         //cout<<"cid "<<cid<<" clustersize "<<clustersize<<endl;
	 cid++;
       }
    }
/*

    for(mm=xbcpos.begin(); mm != xbcpos.end(); mm++){
	if(mm->first == 0) continue;
	cout<<" XXX cluster "<<mm->first<<endl;
	pair<posclass, posclass> mp = mm->second;
	posclass x = mp.first;
	posclass b = mp.second;
	cout<<" lpos "<<x.l<<" mpos "<<x.m<<" rpos "<<x.r<<" nCC "<<x.rs<<endl;
	cout<<" bclpos "<<b.l<<" bcmpos "<<b.m<<" bcrpos "<<b.r<<" rs "<<b.rs<<endl;
	cout<<" lsize "<<xsizes[mm->first].l<<" msize "<<xsizes[mm->first].m<<" rsize "<<xsizes[mm->first].r<<" lastbc "<<xsizes[mm->first].rs<<endl;
    }
*/

}
/*
void adj(uint64_t v, set<uint64_t> &outlinks, map<uint64_t, pair<uint64_t, uint64_t> > &xbcpos, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
    rrr_vector<63>::select_1_type rrrbB_sel(&rrrb_B);
    rrr_vector<63>::rank_1_type rrrbB_rank(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbbc_sel(&rrrb_bc);
    rrr_vector<63>::rank_1_type rrrbbc_rank(&rrrb_bc);

    uint64_t count = wmint.rank(wmint.size(), v);
    uint64_t rankB, pseqX;

    map<uint64_t, set<uint64_t> >::iterator mc;
    for(uint64_t i=1; i<=count; i++){
       pseqX = wmint.select(i, v);
       rankB = rrrbB_rank(pseqX+1); // this is the cluster number
       map<uint64_t, set<uint64_t> > cliques;
       computeCliquesCluster(rankB, cliques, xbcpos, rrrb_bc, rrrb_B, wmint, wmbcint);

       for(mc = cliques.begin(); mc != cliques.end(); mc++){
		//cout<<"clique "<<mc->first<<" : ";
		//printSet(mc->second);
		computeVEdges(v, outlinks, mc->second);
       }
    }

}
*/

void printOneAdj(map<uint64_t, set<uint64_t> > &adjlist, int v){
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;

  for(it=adjlist.begin(); it != adjlist.end(); it++){
     if(v == it->first){
        cout<<v<<": ";
	outlinks = it->second;
  	for(si = outlinks.begin(); si != outlinks.end(); si++)
		cout<<*si<<" ";
        cout<<endl;
     }
  }
}


uint64_t printAdjs(map<uint64_t, set<uint64_t> > &adjlist, int print){
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;
  uint64_t r = 0;
  for(it=adjlist.begin(); it != adjlist.end(); it++){
	if(print)
        cout<<it->first<<":";
	outlinks = it->second;
	if(print){
  	   for(si = outlinks.begin(); si != outlinks.end(); si++)
		cout<<" "<<*si;
           cout<<endl;
	}
	r += outlinks.size();
  }

  return r;

}

int main(int argc, char ** argv) {

  if(argc!=9) {
    cerr << "usage: " << argv[0] << " base_file bitmap_type (0: rrr, 1:sdb) wt_type(0:int, 1:wmint, 2:gmr, 3: wtap) indir outdir v all" << endl;

    return 1;
  }

  string indir = argv[4];
  string outdir = argv[5];
  uint64_t v = atoi(argv[6]); // if all is 0 get only adjlist of v, otherwise all contains all nodes and get all graph
  int all = atoi(argv[7]);
  int printflag = atoi(argv[8]);
  int bitmap_type = atoi(argv[2]);
  int wt_type = atoi(argv[3]);

  string infile = string(argv[1]);
  string bcFile = string(argv[1]) + ".Bc";
  string BFile = string(argv[1]) + ".B";
  //cout<<" infile "<<infile<<" bcFile "<<bcFile<<" BFile "<<BFile<<endl;


  rrr_vector<63> rrrb_bc;
  sd_vector<> sdb_bc;
  rrr_vector<63> rrrb_B;
  sd_vector<> sdb_B;
  if(bitmap_type == 0){
	readRRRBitmap(rrrb_bc, indir, bcFile);
	readRRRBitmap(rrrb_B, indir, BFile);
  } else if(bitmap_type == 1){
	readSDBitmap(sdb_bc, indir, bcFile);
	readSDBitmap(sdb_B, indir, BFile);
  } else {
	cerr << "bad bitmap_type value" << endl;
	return 1;
  }


  wt_int<rrr_vector<63>> wtint;
  wt_int<rrr_vector<63>> wtBcint;
  wm_int<rrr_vector<63>> wmint;
  wm_int<rrr_vector<63>> wmBcint;
  if(wt_type == 0){
  	readWISeqs(wtint, wtBcint, indir, infile);
  } else if(wt_type == 1){
  	readWMSeqs(wmint, wmBcint, indir, infile);
  } else {
	cerr << "bad wt_type value" << endl;
	return 1;
  }


  map<uint64_t, set<uint64_t> > adjlist;
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;


  map<uint64_t, set<uint64_t> >::iterator mc;
  map<uint64_t, pair<posclass, posclass> > xbcpos;
  map<uint64_t, posclass > xsizes;
  map<uint64_t, pair<posclass, posclass> >::iterator mit;
  uint64_t recovered = 0;
  double t=0.0;
  ticks= (double)sysconf(_SC_CLK_TCK);
  start_clock();
  if(bitmap_type == 0 && wt_type == 1){ // rrr for bitmaps and wmint for sequences
	computeXBcPos(xbcpos,xsizes, rrrb_bc,rrrb_B, wmint,wmBcint);
	if( all != 0){ // all nodes
	   for(mit = xbcpos.begin(); mit != xbcpos.end(); mit++){
		if(mit->first == 0) continue;
		computeBiCliquesCluster(mit->first, adjlist, xbcpos, xsizes, rrrb_bc, rrrb_B, wmint, wmBcint);
	   }
	}
	recovered = printAdjs(adjlist, printflag);
/*
	for(mc=adjlist.begin(); mc!=adjlist.end(); mc++){
		cout<<mc->first<<": ";
		set<uint64_t> out = mc->second;
		for(si=out.begin(); si!=out.end(); si++)
			cout<<*si<<" ";
		cout<<endl;
	}
*/
  } else {
	cerr << "unsupported combination of bitmap_type and wt_type values" << endl;
	return 1;
  }

        t += stop_clock();
        t *= 1000; // to milliseconds
	if(printflag == 0){
           cout<<"total time (ms) : "<<t<<endl;
           cout<<"arcs recovered : "<<recovered<<endl;
           cout<<"time per link (ms) : "<<t/recovered<<endl;
	}


/*
	double t=0.0;
	uint64_t recovered = 0;
	ticks= (double)sysconf(_SC_CLK_TCK);
	start_clock();
	if( all != 0){ // all nodes
	   for(mit = xbcpos.begin(); mit != xbcpos.end(); mit++){
		if(mit->first == xbcpos.size()) break;
		//cout<<" cluster "<<mit->first<<" xpos "<<(mit->second).first<<" bcpos "<<(mit->second).second<<endl;
  		map<uint64_t, set<uint64_t> > cliques;
		computeCliquesCluster(mit->first, cliques, xbcpos, rrrb_bc, rrrb_B, wmint, wmBcint);

		for(mc = cliques.begin(); mc != cliques.end(); mc++){
			//cout<<"clique "<<mc->first<<" : ";
			//printSet(mc->second);
			computeEdges(adjlist, mc->second);
		}


	   }
	   recovered = printAdjs(adjlist, printflag);
        } else {
		adj(v-1, outlinks, xbcpos, rrrb_bc, rrrb_B, wmint, wmBcint);
		cout<<v<<": ";
		printSetPlus1(outlinks);
        }

        t += stop_clock();
        t *= 1000; // to milliseconds
	if(printflag == 0){
           cout<<"total time (ms) : "<<t<<endl;
           cout<<"arcs recovered : "<<recovered<<endl;
           cout<<"time per link (ms) : "<<t/recovered<<endl;
	}
  }
*/

  return 0;
}

