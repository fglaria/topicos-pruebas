#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>
#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/wavelet_trees.hpp>

using namespace std;
using namespace sdsl;

/* Time meassuring */
double ticks;
struct tms t1,t2;

void start_clock() {
        times (&t1);
}

double stop_clock() {
        times (&t2);
        return (t2.tms_utime-t1.tms_utime)/ticks;
}
/* end Time meassuring */



vector<unsigned int> splitToInt(string line, string delims){
   string::size_type bi, ei;
   vector<unsigned int> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
  	ei = line.find_first_of(delims, bi);
    	if(ei == string::npos)
      		ei = line.length();
		string aux = line.substr(bi, ei-bi);
    		words.push_back((unsigned int)atoi(aux.c_str()));
    		bi = line.find_first_not_of(delims, ei);
  	}
  return words;
}

void readBitmapFile(string bfile, vector<uint> &fields){
   string line, word;
   ifstream infile(bfile.c_str());
   while(getline(infile,line)){
        if(line.size() == 0)
                continue;
        fields = splitToInt(line, " ");
   }
   infile.close();
   return;
}

void readRRRBitmap(rrr_vector<63> &rrrb, string indir, string outname ){
  outname = indir + outname + "-rrr-64.sdsl";
  load_from_file(rrrb,outname.c_str());
}

void readSDBitmap(sd_vector<> &sdb, string indir, string outname ){
  outname = indir + outname + "-sdb.sdsl";
  load_from_file(sdb,outname.c_str());
}

void readWISeqs(wt_int<rrr_vector<63> > &wtint, wt_int<rrr_vector<63> > &bcint, string indir, string inname ){
    string wtfile = indir + inname + ".seqX-wtint.sdsl";
    string bcfile = indir + inname + ".seqBC-wtint.sdsl";
    load_from_file(wtint,wtfile.c_str());
    load_from_file(bcint,bcfile.c_str());
}

void readWMSeqs(wm_int<rrr_vector<63> > &wmint, wm_int<rrr_vector<63> > &bcm, string indir, string inname ){
    string wtfile = indir + inname + ".seqX-wmint.sdsl";
    string bcfile = indir + inname + ".seqBC-wmint.sdsl";
    load_from_file(wmint,wtfile.c_str());
    load_from_file(bcm,bcfile.c_str());
}

void computeEdges(map<uint64_t, set<uint64_t> > &adjlist, set<uint64_t> &cliques){
    set<uint64_t>::iterator si, si1;
    map<uint64_t, set<uint64_t> >::iterator it;
    for(si=cliques.begin(); si != cliques.end(); si++){
            set<uint64_t> outlist;
            it = adjlist.find(*si);
            if (it != adjlist.end()){
                outlist = it->second;
            }
    	    for(si1=cliques.begin(); si1 != cliques.end(); si1++){
                if(*si != *si1)
                        outlist.insert(*si1);
            }
            adjlist[*si] = outlist;
    }
}

void computeVEdges(uint64_t v, set<uint64_t> &outlinks, set<uint64_t> &cliques){
    set<uint64_t>::iterator si, si1;
    for(si=cliques.begin(); si != cliques.end(); si++){
    	    for(si1=cliques.begin(); si1 != cliques.end(); si1++){
		if(*si != v && *si1 != v) continue;
                if(*si != *si1)
                        outlinks.insert(*si1);
            }
    }
    outlinks.erase(v);
}

void printSet(set<uint64_t> &s){
  set<uint64_t>::iterator si;
  for(si = s.begin(); si != s.end(); si++)
	cout<<*si<<" ";
  cout<<endl;

}

void printSetPlus1(set<uint64_t> &s){
  set<uint64_t>::iterator si;
  for(si = s.begin(); si != s.end(); si++)
	cout<<*si+1<<" ";
  cout<<endl;

}

void computeCliquesCluster(int cluster, map< uint64_t, set<uint64_t> > &cliques, map<uint64_t, pair<uint64_t,uint64_t > > &xbcpos, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
	map<uint64_t, set<uint64_t> >::iterator it;
	pair<uint64_t, uint64_t> startx_bc = xbcpos[cluster];
	pair<uint64_t, uint64_t> endx_bc = xbcpos[cluster+1];
	uint64_t startx = startx_bc.first;
	uint64_t start = startx_bc.second;
	uint64_t endx = endx_bc.first;
	uint64_t end = endx_bc.second;
	if(startx > endx)
		endx = wmint.size()-1;
	if(start > end )
		end = rrrb_bc.size();
	int nCC = wmbcint[cluster-1];
	//cout<<"startx "<<startx<<" endx "<<endx<<" startbc "<<start<<" endbc "<<end<<" ncc "<<nCC<<endl;

 	if(nCC == 1){
		set<uint64_t> oneclique;
		for(uint64_t i=startx; i<endx; i++)
			oneclique.insert(wmint[i]);
		cliques[0] = oneclique;
		return;
	}


	uint64_t j = 0;
	uint64_t symbol;
	for(uint64_t i=start; i<end; i++){
		uint64_t bcval = rrrb_bc[i];
		if (((i-start) % nCC) == 0) { // starts pos of next seqX symbol
			symbol = wmint[startx];
			j=0;
			//cout<<" i "<<i<<" j "<<j<<" startx "<<startx<<" symbol "<<symbol<<endl;
			set<uint64_t> oneclique;
			if(bcval == 1){
				it = cliques.find(j);
				if( it != cliques.end()){
					oneclique = it->second;
				}
				oneclique.insert(symbol);
				cliques[j] = oneclique;
				//cout<<" symbol "<<symbol<<" printing clique j "<<j<<" : ";
				//printSet(oneclique);
			}
		} else {
			set<uint64_t> oneclique;
			if(bcval == 1){
				//cout<<" i "<<i<<" j "<<j<<" startx "<<startx<<" symbol "<<symbol<<endl;
				it = cliques.find(j);
				if( it != cliques.end()){
					oneclique = it->second;
				}
				oneclique.insert(symbol);
				cliques[j] = oneclique;
				//cout<<" else printing clique j "<<j<<" : ";
				//printSet(oneclique);
			}
			if(j == nCC - 1)
				startx++;
		}
		j++;
	}
}

void computeXBcPos(map<uint64_t, pair<uint64_t, uint64_t> > &xbcpos, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
    rrr_vector<63>::rank_1_type rrrbB_rank(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbB_sel(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbbc_sel(&rrrb_bc);
    rrr_vector<63>::rank_1_type rrrbbc_rank(&rrrb_bc);
    uint64_t count = rrrbB_rank(rrrb_B.size());
    //cout<<"count "<<count<<endl;
    for(uint64_t i=1; i<=count; i++){
	xbcpos[i] = make_pair(0,0);
    }
    uint64_t nCC; // number of cliques in this cluster
    uint64_t bcpos;
    uint64_t xpos;

    uint64_t prevcsize = 0, clustersize = 0;

    for(uint64_t i=2; i<=count; i++){
       nCC = wmbcint[i-2]; // number of cliques in this cluster
       //cout<<"i "<<i<<" nCC "<<nCC<<endl;
       prevcsize = clustersize + prevcsize;
       clustersize = rrrbB_sel(i) - prevcsize; // number of symbols in this cluster
       xpos = rrrbB_sel(i);
       bcpos = nCC*clustersize + xbcpos[i-1].second;
       //cout<<" xpos "<<xpos<<" bcpos "<<bcpos<<endl;
       pair<uint64_t,uint64_t> xp(xpos, bcpos);
       //xbcpos[i] = make_pair(xpos, bcpos);
       xbcpos[i] = xp;
    }
}

void adj(uint64_t v, set<uint64_t> &outlinks, map<uint64_t, pair<uint64_t, uint64_t> > &xbcpos, rrr_vector<63> &rrrb_bc, rrr_vector<63> &rrrb_B,  wm_int<rrr_vector<63>> &wmint, wm_int<rrr_vector<63>> &wmbcint){
    rrr_vector<63>::select_1_type rrrbB_sel(&rrrb_B);
    rrr_vector<63>::rank_1_type rrrbB_rank(&rrrb_B);
    rrr_vector<63>::select_1_type rrrbbc_sel(&rrrb_bc);
    rrr_vector<63>::rank_1_type rrrbbc_rank(&rrrb_bc);

    uint64_t count = wmint.rank(wmint.size(), v);
    uint64_t rankB, pseqX;

    map<uint64_t, set<uint64_t> >::iterator mc;
    for(uint64_t i=1; i<=count; i++){
       pseqX = wmint.select(i, v);
       rankB = rrrbB_rank(pseqX+1); // this is the cluster number
       map<uint64_t, set<uint64_t> > cliques;
       computeCliquesCluster(rankB, cliques, xbcpos, rrrb_bc, rrrb_B, wmint, wmbcint);

       for(mc = cliques.begin(); mc != cliques.end(); mc++){
		//cout<<"clique "<<mc->first<<" : ";
		//printSet(mc->second);
		computeVEdges(v, outlinks, mc->second);
       }
    }

}

void printOneAdj(map<uint64_t, set<uint64_t> > &adjlist, int v){
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;

  for(it=adjlist.begin(); it != adjlist.end(); it++){
     if(v == it->first){
        cout<<v<<": ";
	outlinks = it->second;
  	for(si = outlinks.begin(); si != outlinks.end(); si++)
		cout<<*si<<" ";
        cout<<endl;
     }
  }
}


uint64_t printAdjs(map<uint64_t, set<uint64_t> > &adjlist, int print){
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;
  uint64_t r = 0;
  for(it=adjlist.begin(); it != adjlist.end(); it++){
	if(print)
        cout<<it->first<<":";
	outlinks = it->second;
	if(print){
  	   for(si = outlinks.begin(); si != outlinks.end(); si++)
		cout<<" "<<*si;
           cout<<endl;
	}
	r += outlinks.size();
  }

  return r;

}

int main(int argc, char ** argv) {

  if(argc!=9) {
    cerr << "usage: " << argv[0] << " base_file bitmap_type (0: rrr, 1:sdb) wt_type(0:int, 1:wmint, 2:gmr, 3: wtap) indir outdir v all" << endl;

    return 1;
  }

  string indir = argv[4];
  string outdir = argv[5];
  uint64_t v = atoi(argv[6]); // if all is 0 get only adjlist of v, otherwise all contains all nodes and get all graph
  int all = atoi(argv[7]);
  int printflag = atoi(argv[8]);
  int bitmap_type = atoi(argv[2]);
  int wt_type = atoi(argv[3]);

  string infile = string(argv[1]);
  string bcFile = string(argv[1]) + ".Bc";
  string BFile = string(argv[1]) + ".B";
  //cout<<" infile "<<infile<<" bcFile "<<bcFile<<" BFile "<<BFile<<endl;


  rrr_vector<63> rrrb_bc;
  sd_vector<> sdb_bc;
  rrr_vector<63> rrrb_B;
  sd_vector<> sdb_B;
  if(bitmap_type == 0){
	readRRRBitmap(rrrb_bc, indir, bcFile);
	readRRRBitmap(rrrb_B, indir, BFile);
  } else if(bitmap_type == 1){
	readSDBitmap(sdb_bc, indir, bcFile);
	readSDBitmap(sdb_B, indir, BFile);
  } else {
    cerr << "bad bitmap_type value" << endl;
    return 1;
  }

  wt_int<rrr_vector<63>> wtint;
  wt_int<rrr_vector<63>> wtBcint;
  wm_int<rrr_vector<63>> wmint;
  wm_int<rrr_vector<63>> wmBcint;
  if(wt_type == 0){
  	readWISeqs(wtint, wtBcint, indir, infile);
  } else if(wt_type == 1){
  	readWMSeqs(wmint, wmBcint, indir, infile);
  } else {
    cerr << "bad wt_type value" << endl;
    return 1;
  }


  map<uint64_t, set<uint64_t> > adjlist;
  map<uint64_t, set<uint64_t> >::iterator it;
  set<uint64_t> outlinks;
  set<uint64_t>::iterator si;


  map<uint64_t, set<uint64_t> >::iterator mc;
  map<uint64_t, pair<uint64_t, uint64_t> > xbcpos;
  map<uint64_t, pair<uint64_t, uint64_t> >::iterator mit;
  if(bitmap_type == 0 && wt_type == 1){ // rrr for bitmaps and wmint for sequences
	computeXBcPos(xbcpos,rrrb_bc,rrrb_B, wmint,wmBcint);

	double t=0.0;
	uint64_t recovered = 0;
	ticks= (double)sysconf(_SC_CLK_TCK);
	start_clock();
	if( all != 0){ // all nodes
	//cout<<" number cluster "<<xbcpos.size()<<endl;
	   for(mit = xbcpos.begin(); mit != xbcpos.end(); mit++){
		if(mit->first == xbcpos.size()) break;
		//cout<<" cluster "<<mit->first<<" xpos "<<(mit->second).first<<" bcpos "<<(mit->second).second<<endl;
  		map<uint64_t, set<uint64_t> > cliques;
		computeCliquesCluster(mit->first, cliques, xbcpos, rrrb_bc, rrrb_B, wmint, wmBcint);

		for(mc = cliques.begin(); mc != cliques.end(); mc++){
			//cout<<"clique "<<mc->first<<" : ";
			//printSet(mc->second);
			computeEdges(adjlist, mc->second);
		}


	   }
	   recovered = printAdjs(adjlist, printflag);
        } else {
		adj(v-1, outlinks, xbcpos, rrrb_bc, rrrb_B, wmint, wmBcint);
		cout<<v<<": ";
		printSetPlus1(outlinks);
        }

        t += stop_clock();
        t *= 1000; // to milliseconds
	if(printflag == 0){
           cout<<"total time (ms) : "<<t<<endl;
           cout<<"arcs recovered : "<<recovered<<endl;
           cout<<"time per link (ms) : "<<t/recovered<<endl;
	}

  } else {
    cerr << "unsupported combination of bitmap_type and wt_type values" << endl;
    return 1;
  }


  return 0;
}

