#include <stdio.h>
#include <stdlib.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>
#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/wavelet_trees.hpp>

using namespace std;
using namespace sdsl;

vector<unsigned int> splitToInt(string line, string delims){
   string::size_type bi, ei;
   vector<unsigned int> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
  	ei = line.find_first_of(delims, bi);
    	if(ei == string::npos)
      		ei = line.length();
		string aux = line.substr(bi, ei-bi);
    		words.push_back((unsigned int)atoi(aux.c_str()));
    		bi = line.find_first_not_of(delims, ei);
  	}
  return words;
}

void readBitmapFile(string bfile, vector<uint> &fields){
   string line, word;
   ifstream infile(bfile.c_str());
   while(getline(infile,line)){
        if(line.size() == 0)
                continue;
        fields = splitToInt(line, " ");
   }
   infile.close();
   return;
}

double createBitmap(int bitmap_type, bit_vector &b, string outdir, string outname ){
  double bsize = 0.0;
  if(bitmap_type == 0){
  	rrr_vector<63> rrrb(b);
  	cout << "rrr bitmap size "<<size_in_mega_bytes(rrrb) << endl;
  	cout << "rrr bitmap size in bits "<<size_in_bytes(rrrb)*8 << endl;
	bsize = size_in_mega_bytes(rrrb)*1024*1024*8;
        outname = outdir + outname + "-rrr-64.sdsl";
        store_to_file(rrrb,outname.c_str());
  } else if(bitmap_type == 1){
  	sd_vector<> sdb(b);
  	cout << "sdb bitmap size "<<size_in_mega_bytes(sdb) << endl;
	bsize = size_in_mega_bytes(sdb)*1024*1024*8;
        outname = outdir + outname + "-sdb.sdsl";
        store_to_file(sdb,outname.c_str());
  }
  return bsize;
}

double createSeq(int wt_type, string outname, string inname ){
  double ssize = 0.0;
  string wtfile;
  cout<<" inname "<<inname<<" outname "<<wtfile<<endl;
  if(wt_type == 0){
    wt_int<rrr_vector<63>> wtint;
    construct(wtint, inname.c_str(), 4);
    cout << "wtint.size()="<< wtint.size() << " in mega bytes = "<<size_in_mega_bytes(wtint)<<endl;
    cout << "wtint.sigma ="<< wtint.sigma << endl;
    ssize = size_in_mega_bytes(wtint)*1024*1024*8;
    wtfile = outname + "-wtint.sdsl";
    store_to_file(wtint,wtfile.c_str());
  } else if(wt_type == 1){
    wm_int<rrr_vector<63>> wmint;
    construct(wmint, inname.c_str(), 4);
    cout << "inname "<<inname<<" wmint.size()="<< wmint.size() << " in mega bytes = "<<size_in_mega_bytes(wmint)<<endl;
    ssize = size_in_mega_bytes(wmint)*1024*1024*8;
    cout << "wmint.sigma ="<< wmint.sigma << endl;
    wtfile = outname + "-wmint.sdsl";
    store_to_file(wmint,wtfile.c_str());
  } else if(wt_type == 2){
    wt_gmr<> wtgmr;
    construct(wtgmr, inname.c_str(), 4);
    cout << "wtgmr.size()="<< wtgmr.size() << " in mega bytes = "<<size_in_mega_bytes(wtgmr)<<endl;
    ssize = size_in_mega_bytes(wtgmr)*1024*1024*8;
    cout << "wtgmr.sigma ="<< wtgmr.sigma << endl;
    wtfile = outname + "-wtgmr.sdsl";
    store_to_file(wtgmr,wtfile.c_str());
  } else if(wt_type == 3){
    wt_ap<> wtap;
    construct(wtap, inname.c_str(), 4);
    cout << "wtap.size()="<< wtap.size() << " in mega bytes = "<<size_in_mega_bytes(wtap)<<endl;
    cout << "wtap.sigma ="<< wtap.sigma << endl;
    ssize = size_in_mega_bytes(wtap)*1024*1024*8;
    wtfile = outname + "-wtap.sdsl";
    store_to_file(wtap,wtfile.c_str());
  }
  return ssize;
}

int main(int argc, char ** argv) {

  if(argc!=7) {
    //cout << "usage: " << argv[0] << " seqfile bitmapPosFile bitmap_type (0: rrr, 1:sdb) wt_type(0:int, 1:wmint, 2:gmr, 3: wtap) seqlen indir outdir totalEdgesSeq" << endl;
    cerr << "usage: " << argv[0] << "base_file bitmap_type (0: rrr, 1:sdb) wt_type(0:int, 1:wmint, 2:gmr, 3: wtap) indir outdir totalEdgesSeq" << endl;

    return 1;
  }

  string indir = argv[4];
  string outdir = argv[5];
  int totalEdges = atoi(argv[6]);
  int bitmap_type = atoi(argv[2]);
  int wt_type = atoi(argv[3]);

  string inseqfile = string(argv[1]) + ".seqX.bin";
  string inseqbcfile = string(argv[1]) + ".seqBC.bin";
  string bcFile = string(argv[1]) + ".Bc";
  string BFile = string(argv[1]) + ".B";
  inseqfile = indir + inseqfile;
  inseqbcfile = indir + inseqbcfile;
  bcFile = indir + bcFile;
  BFile = indir + BFile;
  cout<<" inseqfile "<<inseqfile<<" bcFile "<<bcFile<<" BFile "<<BFile<<" inseqbcfile "<<inseqbcfile<<" wt_type "<<wt_type<<endl;

  vector<uint> fields;
  readBitmapFile(bcFile, fields);
  size_t bclen = fields.size();
  cout<<" bclen "<<bclen<<endl;

  bit_vector bc = bit_vector(bclen, 0);
  for (size_t i=0; i <= bclen; i++){
 	if(fields[i] == 1)
		bc[i] = 1;
  }
  cout << " Bc bitmap size "<<size_in_mega_bytes(bc) << endl;
  vector<uint> Bfields;
  readBitmapFile(BFile, Bfields);
  size_t Blen = Bfields.size();
  cout<<" Blen "<<Blen<<endl;

  bit_vector B = bit_vector(Blen, 0);
  for (size_t i=0; i <= Blen; i++){
 	if(Bfields[i] == 1)
		B[i] = 1;
  }
  cout << " B bitmap size "<<size_in_mega_bytes(B) << endl;
  string wtfile = string(argv[1]) + ".seqX";
  string wtbcfile = string(argv[1]) + ".seqBC";
  string bcname = string(argv[1]) + ".Bc";
  string Bname = string(argv[1]) + ".B";
  double bcsize = createBitmap(bitmap_type, bc, outdir, bcname);
  double Bsize = createBitmap(bitmap_type, B, outdir, Bname);

  double seqXsize = createSeq(wt_type, outdir + wtfile, inseqfile);
  double seqBcsize = createSeq(wt_type, outdir + wtbcfile, inseqbcfile);


  cout<<"total Bc bits "<<bcsize<<endl;
  cout<<"total B bits "<<Bsize<<endl;
  cout<<"total seq bits "<<seqXsize<<endl;
  cout<<"total seqBc bits "<<seqBcsize<<endl;
  cout<<"total Bc + B + seq bits "<<bcsize + Bsize + seqXsize + seqBcsize<<endl;
  cout<<"total edges in seq "<<totalEdges<<endl;
  cout<<"bpe "<<(seqXsize + seqBcsize + bcsize + Bsize)/totalEdges<<endl;

  return 0;
}

