#ifndef SRC_DENSE_SUB_GRAPHS_MINER_HPP_INCLUDED
#define SRC_DENSE_SUB_GRAPHS_MINER_HPP_INCLUDED

namespace odsg {


class Dag;
class DagNode;
class DenseSubGraph;
class DenseSubGraphsMaximalSet;
class MinerDagTraveler;
class MinerObjective;

/*
 * Define the general, basic, approach to mining dense subgraphs from a dag.
 *
 * The lifetime of the arguments passed as pointers must extend past any use of the respective DenseSubGraphsMiner
 * object.
 *
 * Between the dense subgraphs returned by mine() there is no any with some of their components (sources or centers)
 * with a size equal to 1: internally, these are dropped just at the time they are found. It's because they don't
 * provide value for graph compression, the main objective to achieve with the help of this library.
 *  TODO: Provide an option for not dropping these dense subgraphs, for others contexts where we have starting to use
 * this library, as the bio-odsg biological application for prediction of proteins complexes.
 */
class DenseSubGraphsMiner {
public:
    DenseSubGraphsMiner(const Dag*, MinerDagTraveler*, MinerObjective*,
                        bool asCliques, unsigned long arcsCount, bool noCompressibles=false);

    DenseSubGraphsMaximalSet mine() const;

private:
    const Dag* const dag;
    MinerDagTraveler* const parentTraveler;     // Currently only used during construction
    MinerObjective* const minerObjective;
    bool asCliquesOnly;
    unsigned long minArcsCount;
    bool includeNoCompressibles;

    // Helpers for mine()
    bool willNotProvideEnoughGoodDsg(const DagNode*) const;
    DenseSubGraph getDenseSubGraphFrom(const DagNode*) const;
    bool isGoodEnough(const DenseSubGraph&) const;
};


}       // namespace odsg
#endif  // SRC_DENSE_SUB_GRAPHS_MINER_HPP_INCLUDED
