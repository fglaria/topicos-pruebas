#include "MinerObjective.hpp"

#include <cassert>      // Support run-time assertions. They can be disabled defining the NDEBUG macro

#include "utils/algorithms.hpp"
#include "DenseSubGraph.hpp"

namespace odsg {


//// AsCliqueMinerObjective ///////////////////////////////////////////////////////////////////////////////////////////

/*
 * Note that the 'current' parameter isn't used in non-debug builds, so the ((__unused__)) attribute prevents
 * the respective warning from gcc (or clang) for that cases:
 *   https://gcc.gnu.org/onlinedocs/gcc/Common-Variable-Attributes.html
 */
bool
AsCliqueMinerObjective::better(const DenseSubGraph& current __attribute__ ((__unused__)),
                               const DenseSubGraph& candidate) const {

    assert(current.getCenters().size() < candidate.getCenters().size());
    return algorithms::set_includes(candidate.getSources(), candidate.getCenters());
}


bool
AsCliqueMinerObjective::best(const DenseSubGraph& candidate) const {
    return candidate.getCenters().size() == candidate.getSources().size();
}


//// LegacyMinerObjective /////////////////////////////////////////////////////////////////////////////////////////////

bool
LegacyMinerObjective::better(const DenseSubGraph& current,
                             const DenseSubGraph& candidate) const {
    return current.arcsCount() < candidate.arcsCount();
}


//// MaxIntersectionMinerObjective ////////////////////////////////////////////////////////////////////////////////////

bool
MaxIntersectionMinerObjective::better(const DenseSubGraph& current,
                                      const DenseSubGraph& candidate) const {
    return algorithms::set_intersection_count(current.getCenters(), current.getSources()) <
           algorithms::set_intersection_count(candidate.getCenters(), candidate.getSources());
}

bool
MaxIntersectionMinerObjective::best(const DenseSubGraph& candidate) const {
    // The difference of one is for cases where the mining start from a node who doesn't contain to itself in
    // its node.vertexes set.
    return candidate.getCenters().size() - 1 == candidate.getSources().size();
}


}   // namespace odsg
