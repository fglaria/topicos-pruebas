#ifndef SRC_DAG_VOLATILE_FOREST_HPP_INCLUDED
#define SRC_DAG_VOLATILE_FOREST_HPP_INCLUDED

#include <cstddef>      // NULL, std::size_t
#include <vector>

#include "Dag.hpp"             // All of our container-like classes include the definition of the contained element
#include "GraphCluster.hpp"

namespace odsg {


class Graph;

/*
 * Same as DagForest but it doesn't keep each dag saved in memory. It's now responsability of the caller doing
 * 'delete' in the returned object.
 *
 * It was built for (and only for) huge social/web graphs, due to concerns with memory usage.
 */
class DagVolatileForest {
public:

    // Constructors
    explicit DagVolatileForest(const Graph*,
                               int clusteringScheme=0,
                               unsigned int minClusterSize=1,       // With 'size' we refers to the number of arcs
                               bool sortClusterByFrequency=false);  // It can throw an exception

    const Dag* next();
    void reset();

    // Inspectors
    bool empty() const { return partition.empty(); }
    std::size_t size() const { return partition.size(); }

private:
    const Graph* const graph;
    std::vector<GraphCluster> partition;
    std::vector<GraphCluster>::const_iterator nextCluster;
    bool clusterFrequencySorting;
};


}       // namespace odsg
#endif  // SRC_DAG_VOLATILE_FOREST_HPP_INCLUDED
