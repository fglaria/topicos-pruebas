#!/bin/bash
#
# Extract from a full Boost distribution directory the minimum subset of files required by our project, and
# placed them in include/. It let us to distribute our project without requiring Boost as an external dependence.
#
# It must be run after each #include of a new boost library not used until then. Also, if a library is not required
# anymore, it's recommended to run again this script before distribution too, to ensure that only the strictly
# required files are kept.
#
# The script is basically a wrapper for the bcp utility from the Boost tools:
#   http://www.boost.org/doc/libs/1_58_0/tools/bcp/doc/html/index.html
#
# This script works under the assumption that only header-only libraries are used; it applies to 'most' of the Boost
# libraries:
#   http://www.boost.org/doc/libs/1_58_0/more/getting_started/unix-variants.html#header-only-libraries
#
#
# bcp supports libraries that must be built too, but in these cases this script only warns about that, as the needed
# integration work hasn't been investigated.
# WARN: Currently, due to the use of boost::indirect_iterator by GraphCluster class, bcp generates 2 files to build:
# they can be (manually) deleted, as they are useful only for debugging:
#   http://lists.boost.org/Archives/boost/2002/11/39986.php
#
# --TO CHANGE--
# For now, the script assumes that bcp is placed in the default destination after compiling that tool, i.e.
# ${BOOST_ROOT}/dist/bin/. To do it from scratch: download boost, untar it and run in the extracted directory:
#   ./bootstrap.sh && ./b2 tools/bcp


readonly SCRIPT_PATH=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)  # OK as long as it isn't run as a symbolic link
readonly PROJECT_PATH="${SCRIPT_PATH}/.."

# Sources in our project that could include boost headers; paths are relative to the root of our project.
# Contents of src/utils/ are skipped by design: they depends only on the standard library.
readonly SCANNED_SOURCES="src/*.?pp tools/*.?pp tools/more/*.?pp tests/*.?pp"


usage() {
    PROGNAME=`basename $0`
    cat <<EOF
Usage:  ${PROGNAME} BOOST_ROOT

where:
  BOOST_ROOT    Path to a Boost root directory, e.g.
                /usr/local/boost_1_58_0
EOF
}


fail() {
    echo "error: $1" >&2
    exit 1
}


#### Start of the script ##############################################################################################

cd "${PROJECT_PATH}" || exit 1


if [[ $# != 1 ]]; then
    usage
    exit 1
fi

BOOST_ROOT=$1
[[ -d ${BOOST_ROOT} ]] || fail "BOOST_ROOT doesn't exist or it isn't a directory"

BCP_BIN="${BOOST_ROOT}/dist/bin/bcp"
[[ -x ${BCP_BIN} ]] || fail "bcp tool doesn't exist or it isn't executable"


rm -rf include/boost || exit 1
"${BCP_BIN}" --scan --unix-lines --boost="${BOOST_ROOT}" ${SCANNED_SOURCES} include/


# Warn about the use of non-only-header libraries
if [[ -d "include/libs" ]]; then
    echo "--"
    echo "warning: source or build files were found in 'include/libs'!"
    echo "This script doesn't know how to deal with them..."
fi
