/*
 * CATCH_CONFIG_RUNNER marks this file to host all the implementation sections of the Catch header.
 * CATCH_CONFIG_CONSOLE_WIDTH makes the output for console to fit nicely within the specified number of characters.
 *
 * For more details and other supported options, see:
 *   https://github.com/philsquared/Catch/blob/v1.2.1/docs/configuration.md
 */
#define CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_CONSOLE_WIDTH 120

#include <catch.hpp>


/*
 * Catch provides his own implementation for main(), including the processing of the command line to support
 * a wide set of options; run it with -h or --help for details.
 *
 * To tweak this implementation, see:
 *   https://github.com/philsquared/Catch/blob/v1.2.1/docs/own-main.md
 */
int
main(int argc, char* argv[]) {
    return Catch::Session().run(argc, argv);
}
