#ifndef TESTS_TO_STRING_HPP_INCLUDED
#define TESTS_TO_STRING_HPP_INCLUDED

#include <string>
#include <set>
#include <sstream>

namespace Catch {


/*
 * Overload to let Catch to convert some types into strings, for reporting purposes:
 *   https://github.com/philsquared/Catch/blob/v1.2.1/docs/tostring.md
 *
 * It isn't defined as a more general template to prevent messing-up by accident with some other existent definition
 * of Catch.
 */
template<typename T>
std::string
toString(const std::set<T>& aset) {
    std::ostringstream oss;

    oss << '{';
    for (auto it = aset.cbegin(); it != aset.cend(); ++it) {
        if (it != aset.cbegin())
            oss << ", ";
        oss << *it;
    }
    oss << '}';

    return oss.str();
}


}       // namespace Catch
#endif  // TESTS_TO_STRING_HPP_INCLUDED
