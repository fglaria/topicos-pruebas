/*
 * Unit tests for the general utilities from odsg::algorithms.
 * Many utils from it are mere shortcuts for the correspondent STL algorithms; only a few of these are tested here.
 */
#include <utils/algorithms.hpp>

#include <string>
#include <vector>
#include <set>

#include "toString.hpp"     // It must be included *before* catch.hpp
#include <catch.hpp>

using namespace odsg;


TEST_CASE("algorithms::binary_find", "[utils][algorithms]") {
    std::vector<int> empty;
    CHECK(algorithms::binary_find(empty.begin(), empty.end(), 1337) == empty.end());

    const std::vector<int> fib{0, 1, 1, 2, 3, 5, 8};
    CHECK(algorithms::binary_find(fib.begin(), fib.end(), 0) == fib.begin());
    CHECK(algorithms::binary_find(fib.begin(), fib.end(), 1) == fib.begin() + 1);
    CHECK(algorithms::binary_find(fib.begin(), fib.end(), 2) == fib.begin() + 3);
    CHECK(algorithms::binary_find(fib.begin(), fib.end(), 8) == fib.begin() + 6);

    const std::vector<int> numbers{1, 3, 3, 3, 5, 5, 6, 8};
    CHECK(algorithms::binary_find(numbers.begin(), numbers.end(), 0) == numbers.end());
    CHECK(algorithms::binary_find(numbers.begin(), numbers.end(), 2) == numbers.end());
    CHECK(algorithms::binary_find(numbers.begin(), numbers.end(), 4) == numbers.end());
    CHECK(algorithms::binary_find(numbers.begin(), numbers.end(), 7) == numbers.end());
    CHECK(algorithms::binary_find(numbers.begin(), numbers.end(), 9) == numbers.end());
}


TEST_CASE("algorithms::set_includes", "[utils][algorithms]") {

    SECTION("with an empty set") {
        std::set<int> empty;
        std::set<int> nonEmpty{2, 4, 7};
        CHECK(algorithms::set_includes(empty, empty));
        CHECK(algorithms::set_includes(nonEmpty, empty));
        CHECK_FALSE(algorithms::set_includes(empty, nonEmpty));
    }
}


TEST_CASE("algorithms::set_intersection", "[utils][algorithms]") {
    std::set<int> results;

    SECTION("with an empty set") {
        std::set<int> empty;
        std::set<int> nonEmpty{2, 4, 7};
        algorithms::set_intersection(empty, empty, results);
        algorithms::set_intersection(empty, nonEmpty, results);
        algorithms::set_intersection(nonEmpty, empty, results);     // results are acummulated
        CHECK(results.size() == 0);
    }

    SECTION("general") {
        const std::set<int> s1{-3, -2, -1, 0, 1};
        const std::set<int> s2{        -1, 0, 1, 2, 3, 4, 5, 6};
        algorithms::set_intersection(s1, s2, results);
        algorithms::set_intersection(s2, s1, results);
        CHECK(results == std::set<int>({-1, 0, 1}));
    }

    // TODO: algorithms::set_intersection(results, results, results)?
}


TEST_CASE("algorithms::set_intersection_count", "[utils][algorithms]") {

    SECTION("with an empty set") {
        std::set<int> empty;
        std::set<int> nonEmpty{2, 4, 7};
        CHECK(algorithms::set_intersection_count(empty, empty) == 0);
        CHECK(algorithms::set_intersection_count(nonEmpty, empty) == 0);
        CHECK(algorithms::set_intersection_count(empty, nonEmpty) == 0);
    }

    SECTION("with itself") {
        std::set<int> me{1, 11, 101};
        CHECK(algorithms::set_intersection_count(me, me) == me.size());
    }

    SECTION("general") {
        const std::set<int> s1{0, 6, 1, 4};
        const std::set<int> s2{      1, 4, 5, 2, 7};
        CHECK(algorithms::set_intersection_count(s1, s2) == 2);
        CHECK(algorithms::set_intersection_count(s2, s1) == 2);
    }
}


TEST_CASE("algorithms::has_unique", "[utils][algorithms]") {
    std::vector<int> empty;
    CHECK(algorithms::has_unique(empty));

    std::vector<std::string> single{"dafne"};
    CHECK(algorithms::has_unique(single));

    std::vector<std::string> musicians{"bach", "mozart", "chopin", "liszt"};
    CHECK(algorithms::has_unique(musicians));

    const std::vector<int> fib{0, 1, 1, 2, 3, 5, 8};
    CHECK_FALSE(algorithms::has_unique(fib));

    const std::vector<char> hulu{'h', 'u', 'l', 'u'};
    CHECK_FALSE(algorithms::has_unique(hulu));
}


TEST_CASE("algorithms::is_sorted", "[utils][algorithms]") {
    std::vector<int> empty;
    CHECK(algorithms::is_sorted(empty));

    std::vector<std::string> single{"aiko"};
    CHECK(algorithms::is_sorted(single));

    const std::vector<int> fib{0, 1, 1, 2, 3, 5, 8};
    CHECK(algorithms::is_sorted(fib));

    std::vector<std::string> permutations{"aspo", "paso", "posa", "sopa", "sapo"};
    CHECK_FALSE(algorithms::is_sorted(permutations));
}
