/*
 * Unit tests for the general string utilities from odsg::strings.
 */
#include <utils/strings.hpp>

#include <string>
#include <vector>

#include <catch.hpp>

using namespace odsg;


TEST_CASE("strings::to_str", "[utils][strings]") {
    CHECK(strings::to_str(0) == "0");
    CHECK(strings::to_str(-350) == "-350");
    CHECK(strings::to_str(2.731500e2) == "273.15");
    CHECK(strings::to_str(0xFF) == "255");
    CHECK(strings::to_str("a literal string") == "a literal string");
    CHECK(strings::to_str(true) == "true");
    CHECK(strings::to_str(false) == "false");
}


TEST_CASE("strings::str_to", "[utils][strings]") {
    CHECK(strings::str_to<int>("0") == 0);
    CHECK(strings::str_to<int>("-350") == -350);
    CHECK(strings::str_to<float>("2.731500e2") == Approx(273.15));
    CHECK(strings::str_to<bool>("1") == true);
    CHECK(strings::str_to<int>("0xFF") == 0);
    CHECK(strings::str_to<std::string>("a literal string") == "a literal string");
}


TEST_CASE("strings::split", "[utils][strings]") {
    std::string empty;
    CHECK(strings::split<int>(empty).empty());

    std::string onlyDelimiters = ",,,,,";
    CHECK(strings::split<int>(onlyDelimiters, ',').empty());

    std::string single = "ruth";
    CHECK(strings::split<std::string>(single, ',') == std::vector<std::string>({"ruth"}));

    std::string PATH = "/usr/local/bin:/usr/bin:/bin:/opt/bin";
    CHECK(strings::split<std::string>(PATH, ':')
          == std::vector<std::string>({"/usr/local/bin", "/usr/bin", "/bin", "/opt/bin"}));

    std::string consecutiveDelimiters = " ha haha   hahahaha       ";
    CHECK(strings::split<std::string>(consecutiveDelimiters, ' ')
          == std::vector<std::string>({"ha", "haha", "hahahaha"}));
}
