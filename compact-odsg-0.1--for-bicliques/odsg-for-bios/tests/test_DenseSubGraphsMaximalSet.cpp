#include <DenseSubGraphsMaximalSet.hpp>

#include <vector>

#include <catch.hpp>

#include <VertexSet.hpp>

using namespace odsg;


/*
 * Note that the base sections use REQUIRE, as tests in deeper sections are dependent of the correctness of these,
 * but deepest sections use CHECK, letting with to continue the execution in case of fail.
 */
TEST_CASE("DenseSubGraphsMaximalSet::insert", "[densesubgraphsmaximalset]") {
    DenseSubGraphsMaximalSet dsgs;
    bool inserted;

    REQUIRE(dsgs.empty());


    SECTION("individually inserting maximal dense subgraphs") {
        std::vector<DenseSubGraph> maximals = { DenseSubGraph({1   }, {4, 5      }),    // (a)
                                                DenseSubGraph({1   }, {   5, 6   }),    // (b)
                                                DenseSubGraph({1, 2}, {4,    6   }),    // (c)
                                                DenseSubGraph({   2}, {   5      }),    // (d)
                                                DenseSubGraph({    }, {4, 5, 6   }),    // (e)
                                                DenseSubGraph({    }, {         7}) };  // (f)
        for (const auto& dsg: maximals) {
            inserted = dsgs.insert(dsg);
            REQUIRE(inserted);
        }
        REQUIRE(dsgs.size() == maximals.size());


        SECTION("trying to insert individually no maximal dense subgraphs") {
            std::vector<DenseSubGraph> noMaximals = { DenseSubGraph({    }, VertexSet{       }),
                                                      DenseSubGraph({1   }, {4      }),
                                                      DenseSubGraph({1   }, {4, 5   }),
                                                      DenseSubGraph({   2}, {4      }),
                                                      DenseSubGraph({    }, {4,    6}) };
            for (const auto& dsg: noMaximals) {
                inserted = dsgs.insert(dsg);
                CHECK_FALSE(inserted);
            }
            CHECK(dsgs.size() == maximals.size());
        }

        SECTION("individually insert maximal dense subgraphs, superseding one existent") {
            std::vector<DenseSubGraph> newMaximals = { DenseSubGraph({   2   }, {4, 5      }),      // Supersede (d)
                                                       DenseSubGraph({   2   }, {         7}),      // Supersede (f)
                                                       DenseSubGraph({1,    3}, {4, 5,    7}) };    // Supersede (a)
            for (const auto& dsg: newMaximals) {
                inserted = dsgs.insert(dsg);
                CHECK(inserted);
            }
            CHECK(dsgs.size() == maximals.size());
        }

        SECTION("one dense subgraph to supersede them all") {
            inserted = dsgs.insert(DenseSubGraph({0, 1, 2}, {4, 5, 6, 7}));
            CHECK(inserted);
            CHECK(dsgs.size() == 1);
        }
    }

    // TODO: The same for as-cliques dense subgraphs
}
