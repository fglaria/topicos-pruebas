#include <DenseSubGraph.hpp>

#include <sstream>

#include "toString.hpp"     // It must be included *before* catch.hpp
#include <catch.hpp>

#include <utils/strings.hpp>
#include <VertexSet.hpp>

using namespace odsg;


TEST_CASE("DenseSubGraph::get*", "[densesubgraph]") {
    DenseSubGraph empty;
    CHECK(empty.getSources().empty());
    CHECK(empty.getCenters().empty());

    DenseSubGraph noSources({}, {2, 3});
    CHECK(noSources.getSources().empty());

    DenseSubGraph noCenters({2, 3}, VertexSet{});   // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(noCenters.getCenters().empty());

    VertexSet vxset1{2, 3, 5, 7, 8};
    VertexSet vxset2{7, 8, 9};
    DenseSubGraph dsg(vxset1, vxset2);
    CHECK(dsg.getSources() == vxset1);
    CHECK(dsg.getCenters() == vxset2);
}


TEST_CASE("DenseSubGraph: comparators", "[densesubgraph]") {
    DenseSubGraph empty;
    DenseSubGraph dsg1({4, 5, 6}, {2, 3});
    DenseSubGraph dsg2({4, 5, 6}, {2, 3});
    DenseSubGraph dsg3({2, 3}, {4, 5, 6});
    DenseSubGraph dsg4({4, 5, 6}, {4, 5, 6});
    CHECK(empty == empty);
    CHECK(dsg1 == dsg1);
    CHECK(dsg1 == dsg2);
    CHECK(dsg2 == dsg1);
    CHECK(dsg1 != dsg3);
    CHECK(dsg1 != dsg4);
    CHECK(dsg3 != dsg4);
}


TEST_CASE("DenseSubGraph::DenseSubGraph", "[densesubgraph]") {
    DenseSubGraph empty1;
    DenseSubGraph empty2({}, VertexSet{});  // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(empty1 == empty2);

    DenseSubGraph dsg1({}, 1);
    DenseSubGraph dsg2({}, VertexSet{1});
    CHECK(dsg1 == dsg2);
}


TEST_CASE("DenseSubGraph::merge", "[densesubgraph]") {
    DenseSubGraph dsg1({3, 4, 5, 6}, {2});
    DenseSubGraph dsg2({4, 5, 6}, {4});
    DenseSubGraph dsg3({1, 2, 7, 8}, {7});

    DenseSubGraph selfMerged = dsg1;
    selfMerged.merge(selfMerged);
    CHECK(selfMerged == dsg1);

    dsg1.merge(dsg2);
    CHECK(dsg1 == DenseSubGraph({4, 5, 6}, {2, 4}));
    dsg1.merge(dsg3);
    CHECK(dsg1 == DenseSubGraph({}, {2, 4, 7}));
}


TEST_CASE("DenseSubGraph::swap", "[densesubgraph]") {
    const DenseSubGraph dsg1({3, 4, 5, 6}, {2});
    const DenseSubGraph dsg2({2, 7, 8}, {7, 8, 9});
    const DenseSubGraph dsg3({}, {3, 4, 5, 6, 7, 8, 9});

    DenseSubGraph selfSwap(dsg1);
    CHECK_NOTHROW(selfSwap.swap(selfSwap));

    DenseSubGraph dsg10(dsg1);
    DenseSubGraph dsg20(dsg2);
    DenseSubGraph dsg30(dsg3);
    dsg10.swap(dsg20);
    dsg20.swap(dsg30);
    dsg10.swap(dsg20);
    dsg10.swap(dsg30);
    CHECK(dsg10 == dsg1);
    CHECK(dsg20 == dsg2);
    CHECK(dsg30 == dsg3);
}


TEST_CASE("DenseSubGraph::includes", "[densesubgraph]") {
    DenseSubGraph empty;
    DenseSubGraph dsg1({4, 5, 6}, {2, 3});
    DenseSubGraph dsg2({4, 5, 6}, {2});
    DenseSubGraph dsg3({4, 5}, {2, 3});
    DenseSubGraph dsg4({4, 5, 6}, {4, 5, 6});
    CHECK(empty.includes(empty));
    CHECK(dsg1.includes(dsg1));
    CHECK(dsg1.includes(dsg2));
    CHECK(dsg1.includes(dsg3));
    CHECK_FALSE(dsg1.includes(dsg4));
    CHECK_FALSE(dsg3.includes(dsg1));
}


TEST_CASE("DenseSubGraph::arcsCount", "[densesubgraph]") {
    DenseSubGraph empty;
    CHECK(empty.arcsCount() == 0);

    DenseSubGraph noSources({}, {2, 3});
    CHECK(noSources.arcsCount() == 0);

    DenseSubGraph noCenters({2, 3}, VertexSet{});   // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(noCenters.arcsCount() == 0);

    DenseSubGraph dsg({2, 3, 5, 7, 8}, {7, 8, 9});
    CHECK(dsg.arcsCount() == 15);
    CHECK(dsg.arcsCount(false) == 15);

    DenseSubGraph asClique({2, 3, 5, 8, 9}, {2, 3, 5});
    CHECK(asClique.arcsCount(true) == 9);

    DenseSubGraph clique({2, 3, 5}, {2, 3, 5});
    CHECK(clique.arcsCount(true) == 9);
}


TEST_CASE("DenseSubGraph::noSelfLoopsArcsCount", "[densesubgraph]") {
    DenseSubGraph empty;
    CHECK(empty.noSelfLoopsArcsCount() == 0);

    DenseSubGraph noSources({}, {2, 3});
    CHECK(noSources.noSelfLoopsArcsCount() == 0);

    DenseSubGraph noCenters({2, 3}, VertexSet{});   // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(noCenters.noSelfLoopsArcsCount() == 0);

    DenseSubGraph biClique({2, 3, 5, 7, 8}, {9, 10});
    CHECK(biClique.noSelfLoopsArcsCount() == biClique.arcsCount());

    DenseSubGraph dsg({2, 3, 5, 7, 8}, {7, 8, 9});
    CHECK(dsg.noSelfLoopsArcsCount() == 13);
    CHECK(dsg.noSelfLoopsArcsCount(false) == 13);

    DenseSubGraph clique({2, 3}, {2, 3});
    CHECK(clique.noSelfLoopsArcsCount() == 2);

    DenseSubGraph asClique({2, 3, 5, 8, 9}, {2, 3, 5});
    CHECK(asClique.noSelfLoopsArcsCount(true) == 6);
}


TEST_CASE("DenseSubGraph::asClique", "[densesubgraph]") {
    DenseSubGraph empty;
    CHECK_FALSE(empty.asClique());

    DenseSubGraph clique({3, 4, 5, 6}, {3, 4, 5, 6});
    CHECK_FALSE(clique.asClique());

    DenseSubGraph noCenters({2, 3}, VertexSet{});   // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(noCenters.asClique());

    DenseSubGraph asClique({3, 4, 5, 6}, {4, 5});
    CHECK(asClique.asClique());

    DenseSubGraph dsg1({3, 4, 5}, {3, 4, 5, 6});
    CHECK_FALSE(dsg1.clique());

    DenseSubGraph dsg2({2, 3}, {4, 5});
    CHECK_FALSE(dsg2.asClique());
}


/*
 * strings::to_str is used here, as it deals internally with the streamable form of an object to built the string.
 */
TEST_CASE("DenseSubGraph::operator<<", "[densesubgraph]") {
    DenseSubGraph empty;
    CHECK(strings::to_str(empty) == "-");

    DenseSubGraph minimal({}, {1});
    CHECK(strings::to_str(minimal) == "- 1");

    DenseSubGraph minimal2({1}, VertexSet{});   // TRICKY: VertexSet{} is used to disambiguate constructor
    CHECK(strings::to_str(minimal2) == "1 -");

    DenseSubGraph clique({1, 2, 5, 6}, {1, 2, 5, 6});
    CHECK(strings::to_str(clique) == "1 2 5 6");

    DenseSubGraph dsg({5, 6}, {1, 2, 3, 4, 5, 6});
    CHECK(strings::to_str(dsg) == "5 6 - 1 2 3 4 5 6");
}


TEST_CASE("DenseSubGraph::operator>>", "[densesubgraph]") {

    SECTION("input an empty dense subgraph") {
        std::istringstream iss("<---:");

        DenseSubGraph empty;
        iss >> empty;
        CHECK(empty == DenseSubGraph());
        CHECK(iss.good());  // Ensure good state of the stream for future operations

        // Ensure no extra characters were taken from the stream
        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input an clique") {
        std::istringstream iss("1 2 5 6");

        DenseSubGraph clique;
        iss >> clique;
        CHECK(clique == DenseSubGraph({1, 2, 5, 6}, {1, 2, 5, 6}));
        CHECK(iss.good());

        // TODO: Ensure no extra characters were taken from the stream
    }

    SECTION("input a dense subgraph with no sources") {
        std::istringstream iss("0 <---:");

        DenseSubGraph noSources;
        iss >> noSources;
        CHECK(noSources == DenseSubGraph({}, {0}));
        CHECK(iss.good());

        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input a dense subgraph with no centers") {
        std::istringstream iss("<--- 2 3:");

        DenseSubGraph noCenters;
        iss >> noCenters;
        CHECK(noCenters == DenseSubGraph({2, 3}, VertexSet{}));
        CHECK(iss.good());

        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input a general dense subgraph") {
        std::istringstream iss("1 2 3 4 5 6 <--- 5 6:");

        DenseSubGraph dsg;
        iss >> dsg;
        CHECK(dsg == DenseSubGraph({5, 6}, {1, 2, 3, 4, 5, 6}));
        CHECK(iss.good());

        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input not a dense subgraph") {
        std::istringstream iss("<-");   // Wrong, incomplete delimiter

        DenseSubGraph dsg({3}, {4, 5, 6});
        iss >> dsg;
        // TODO: Ensure bad status of the stream
        //CHECK(dsg == DenseSubGraph({3}, {4, 5, 6}));    // dsg wasn't modified

        // TODO: Ensure no extra characters were taken from the stream
    }

}
