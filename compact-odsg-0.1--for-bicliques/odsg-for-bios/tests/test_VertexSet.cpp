/*
 * For VertexSet, being the same as std::set, only its custom form for in/out streaming is worth testing here.
 */
#include <VertexSet.hpp>

#include <sstream>

#include "toString.hpp"     // It must be included *before* catch.hpp
#include <catch.hpp>

#include <utils/strings.hpp>

using namespace odsg;


/*
 * strings::to_str is used here, as it deals internally with the streamable form of an object to built the string.
 */
TEST_CASE("VertexSet::operator<<", "[vertexset]") {
    VertexSet empty;
    CHECK(strings::to_str(empty) == "");

    VertexSet single{26};
    CHECK(strings::to_str(single) == "26");

    VertexSet vxset{0, 1, 2, 3, 5, 11};
    CHECK(strings::to_str(vxset) == "0 1 2 3 5 11");
}


TEST_CASE("VertexSet::operator>>", "[vertexset]") {

    SECTION("input an empty vertex set") {
        std::istringstream iss(":");

        VertexSet empty;
        iss >> empty;
        CHECK(empty.empty());
        CHECK(iss.good());  // Ensure good state of the stream for future operations

        // Ensure no extra characters were taken from the stream
        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input a vertex set with a single element") {
        std::istringstream iss("26:");

        VertexSet single;
        iss >> single;
        CHECK(single == VertexSet({26}));
        CHECK(iss.good());

        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }

    SECTION("input a general vertex set") {
        std::istringstream iss("0 1 2 3 5 11:");

        VertexSet vxset;
        iss >> vxset;
        CHECK(vxset == VertexSet({0, 1, 2, 3, 5, 11}));
        CHECK(iss.good());

        char c = 0;
        iss >> c;
        CHECK(c == ':');
    }
}
