#include <exception>
#include <string>
#include <iostream>
#include <algorithm>

#include <tclap/CmdLine.h>

#include <utils/algorithms.hpp>
#include <Graph.hpp>

using namespace odsg;


struct CmdLineArgs {    // Check definition of processCmdLine() to get a description of each option
    std::string graphFileName;
    bool comeSorted;
};

CmdLineArgs processCmdLine(int argc, char* argv[]);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct GraphStats {
    explicit GraphStats(const Graph&);

    std::size_t listsCount;
    std::size_t listsCountWithLoops;
    std::size_t nodesCount;
    unsigned long arcsCount;
    unsigned long arcsCountNoLoops;
};

GraphStats::GraphStats(const Graph& g)
: listsCount(0), nodesCount(0), arcsCount(0), arcsCountNoLoops(0) {

    listsCount = g.listsCount();
    nodesCount = g.nodesCount();
    arcsCount = g.arcsCount();

    if (g.isMineable()) {
        // Self-loops are present in all adjacency list
        arcsCountNoLoops = arcsCount - listsCount;
    } else {
        // Self-loops can or can not be present in each adjacency list: figure it in manual way
        for (Graph::const_iterator it = g.begin(); it != g.end(); ++it) {
            std::size_t arcs = it->second.size();

            bool isSelfLoopPresent = g.isSortedByVertex()
                                        ? std::binary_search(it->second.begin(), it->second.end(), it->first)
                                        : algorithms::is_found(it->second, it->first);
            arcsCountNoLoops += isSelfLoopPresent ? arcs - 1 : arcs;
        }
    }

    listsCountWithLoops = arcsCount - arcsCountNoLoops;
}


void
describeGraph(const Graph& graph) {
    GraphStats stats(graph);
    std::cout << "  "
              << stats.nodesCount << " nodes, "
              << stats.arcsCount << " arcs and "
              << stats.listsCount << " adjacency lists";

    if (!graph.isMineable()) {
        // Describe the self-loops manually

        if (stats.arcsCount == stats.arcsCountNoLoops)
            std::cout << ", none of these lists with self-loops";
        else if (stats.arcsCount == stats.arcsCountNoLoops + stats.listsCount)
            std::cout << ", all of these lists with self-loops";
        else
            std::cout << ", " << stats.listsCountWithLoops << " of these lists with self-loops";
    }

    std::cout << std::endl;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int
main(int argc, char* argv[]) {

    CmdLineArgs args;
    try {
        args = processCmdLine(argc, argv);
    } catch (TCLAP::ArgException& e) {
        std::cerr << "error: " << e.argId() << '\n'
                  << "       " << e.error() << std::endl;
        return 1;
    }


    // Build the graph object from the input file
    Graph myGraph;
    try {
        myGraph = Graph(args.graphFileName, args.comeSorted);
    } catch (std::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        return 1;
    }

    std::cout << "The graph has:" << std::endl;
    describeGraph(myGraph);

    // Rebuild the graph for mining
    if (args.comeSorted) {
        myGraph.rebuildForMining();     // Default implementation will skip internally the sorting
    } else {
        myGraph.rebuildForMining(Graph::VertexComparer());      // The faster
    }

    std::cout << "After to be rebuilt for mining, it has:" << std::endl;
    describeGraph(myGraph);

    return 0;
}


/*
 * The next does use of the Templatized C++ Command Line Parser (TCLAP) library, in include/ directory.
 *   http://tclap.sourceforge.net/manual.html
 */
CmdLineArgs
processCmdLine(int argc, char* argv[]) {

    //// Define the main command line object //////////////////////////////////////////////////////////////////////
    TCLAP::CmdLine cmd("Show many statistics about an input graph before and after to be rebuilt for mining"
                            " -- Carlos Mella, Cecilia Hernandez",
                                    // Message to be displayed in the USAGE output
                       ' ',         // Character used to separate the argument flag/name from the value
                       "1",         // Version number to be displayed by the --version switch
                       false);      // Whether or not to create the automatic --help and --version switches


    //// Arguments are separate objects, added to the CmdLine object one at a time ////////////////////////////////

    // Switch args are boolean arguments to define a flag; its presence negates its default value
    TCLAP::SwitchArg sortedArg(
        "s",
        "came-sorted",
        "Indicate that the input graph already have its adjacency lists sorted by increasing id,"
            " reducing with it the total processing time.",
        cmd,
        false);

    // Unlabeled value args aren't identified by a flag, instead they are identified by their position in the argv
    // array. Note that:
    //  - the order that they are added here to the cmd object is the order that they will be parsed.
    //  - only one optional UnlabeledValueArg is possible; it must be the last listed.
    TCLAP::UnlabeledValueArg<std::string> graphFileNameArg(
        "GRAPH_FILE",
        "Path to a input text file defining a graph...",
        true,
        "",
        "GRAPH_FILE",
        cmd);


    //// Parse the argv array /////////////////////////////////////////////////////////////////////////////////////
    cmd.parse(argc, argv);

    // Extra validation checks
    if (graphFileNameArg.getValue().empty())
        throw TCLAP::CmdLineParseException("Empty argument!", graphFileNameArg.longID());


    //// Get the value parsed by each argument ////////////////////////////////////////////////////////////////////
    CmdLineArgs args;

    args.graphFileName = graphFileNameArg.getValue();
    args.comeSorted = sortedArg.getValue();

    return args;
}
