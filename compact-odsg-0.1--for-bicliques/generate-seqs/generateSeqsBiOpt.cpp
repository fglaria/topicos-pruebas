#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <set>

#include "Shingles.hpp"

using namespace std;

template<typename A, typename B>
pair<B,A> flip_pair(const pair<A,B> &p)
{
    return pair<B,A>(p.second, p.first);
}

template<typename A, typename B>
multimap<B,A> flip_map(const map<A,B> &src)
{
    multimap<B,A> dst;
    transform(src.begin(), src.end(), inserter(dst, dst.begin()),
                   flip_pair<A,B>);
    return dst;
}

typedef vector< vector<int > > Sequences;

vector<int> split(string line, string delims){
   string::size_type bi, ei;
   vector<int> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
        ei = line.find_first_of(delims, bi);
        if(ei == string::npos)
                ei = line.length();
                string sv = line.substr(bi, ei-bi);
                int fv = atof(sv.c_str());
                words.push_back(fv);
                //words.insert(fv);
                bi = line.find_first_not_of(delims, ei);
        }
  return words;
}

vector<string> splitStr(string line, string delims){
   string::size_type bi, ei;
   vector<string> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
        ei = line.find_first_of(delims, bi);
        if(ei == string::npos)
                ei = line.length();
                string sv = line.substr(bi, ei-bi);
                words.push_back(sv);
                bi = line.find_first_not_of(delims, ei);
        }
  return words;
}


void
readSequencesFromFile(const char* fileName, vector< pair< vector<int> , vector<int> > > &biclique) {
    ifstream infile(fileName);
    if (infile) {
        string line;
        while (getline(infile, line)) {
            if (line.empty())
                continue;
	    vector<string> twosets = splitStr(line,"-");
	    vector<int> setS, setC;
	    pair< vector<int>, vector<int> > p;
	    if(twosets.size() == 2){ // it is a biclique
	    	setS = split(twosets[0]," ");
		setC = split(twosets[1], " ");
	    } else if(twosets.size() == 1 ) { // it is a clique, must replicate one set
	    	setS = split(twosets[0]," ");
		setC = setS;
	    } else {
		continue;
	    }
	    p.first = setS;
	    p.second = setC;
	    biclique.push_back(p);
        }
    }
}

void printBiclique(vector < pair< vector<int>, vector<int> > > &biclique){
	vector<int>::iterator it;
	for(unsigned int i=0; i<biclique.size(); i++){
		pair< vector<int>, vector<int> > p = biclique[i];
		vector<int> s = p.first;
		vector<int> c = p.second;
		for(it = s.begin(); it!= s.end(); it++)
			cout<<*it<<" ";
		cout<<" - ";
		for(it = c.begin(); it!= c.end(); it++)
			cout<<*it<<" ";

		cout<<endl;
	}
}

void printVector (vector<Shingles::Signature> v){
	for(unsigned int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}

void printSet (set<int> s){
	set<int>::iterator sit;
	for(sit=s.begin(); sit!=s.end(); sit++){
		cout<<*sit<<" ";
	}
	cout<<endl;
}

void printMap (map<int, int> m){
	map<int, int>::iterator mit;
	for(mit = m.begin(); mit != m.end(); mit++){
		cout<<mit->first<<" "<<mit->second<<endl;
	}
}

void printBc (map<int, set<int> > m){
	map<int, set<int> >::iterator mit;
	set<int>::iterator sit;
	for(mit = m.begin(); mit != m.end(); mit++){
		set<int> s = mit->second;
		cout<<mit->first<<": ";
		printSet(s);
	}
}

void appendSeqX(set<int> nodes, ofstream &seq, ofstream &seqbin){
	set<int>::iterator sit;
 	unsigned int *buffer = new unsigned int[nodes.size()];
	int i=0;
	for(sit = nodes.begin(); sit != nodes.end(); sit++){
		seq<<*sit<<" ";
		buffer[i] = *sit;
		i++;
	}
	seqbin.write((char *)&*buffer,(nodes.size())*sizeof(unsigned int));
	delete[] buffer;
}

void appendBc(map<int, set<int> > mapNodeCliques, ofstream &bitmap, int numberCliques, set<int> nodes){
	map<int, set<int> >::iterator mit;
	set<int>::iterator sit, sit1;
	if(mapNodeCliques.size() != nodes.size()){
		cerr<<" error: numberNodes in Seq differs from numberNodes in mapcliques ";
		cerr<<nodes.size()<<" map size "<<mapNodeCliques.size()<<endl;
		exit(1);
	}
	unsigned int totalbits = numberCliques * nodes.size();
	int *bits = new int[totalbits];
	for(unsigned int i=0; i<totalbits; i++)
		bits[i] = 0;

	//for(unsigned int i=0; i<totalbits; i++)
	//		cout<<"bits["<<i<<"] = "<<bits[i]<<endl;

	int j=0;
	for(sit = nodes.begin(); sit != nodes.end(); sit++){ // foreach node there should be numberClusters bits
	        set<int> cliqueids = mapNodeCliques[*sit];
	        for(sit1 = cliqueids.begin(); sit1 != cliqueids.end(); sit1++){
			bits[j+*sit1] = 1;
	        }
		j=j+numberCliques;
	}
	for(unsigned int i=0; i<totalbits; i++)
		bitmap<<bits[i]<<" ";

	delete []bits;
}

void buildBitmapB( vector<int> numberCs, string outdir){
    string Bfilename = outdir + ".B";
    ofstream B;
    B.open(Bfilename.c_str(), std::ofstream::out);
    vector<int> Bitmap;
    Bitmap.push_back(1); // first 1 in Bitmap
    for(unsigned int i=0; i<numberCs.size(); i++){
	//for(int j=0; j<numberCs[i]; j++)
	for(int j=1; j<numberCs[i]; j++)
			Bitmap.push_back(0);
	Bitmap.push_back(1);
    }
    //cout<<"Bitmap"<<endl;
    for(unsigned int i=0; i<Bitmap.size(); i++){
	//cout<<Bitmap[i]<<" ";
	B<<Bitmap[i]<<" ";
    }
    //cout<<endl;
    B.close();
}

int buildSeqNumCliques(vector<int> numCliquesInCluster, string outdir){
    string seqbcfilename = outdir + ".seqBC";
    string seqbcfilenamebin = outdir + ".seqBC.bin";
    ofstream seqbc,seqbcbin;
    seqbc.open(seqbcfilename.c_str(), std::ofstream::out);
    seqbcbin.open(seqbcfilenamebin.c_str(), ofstream::binary);
    unsigned int *buffer = new unsigned int[numCliquesInCluster.size()];
    int totalCliques = 0;
    for(unsigned int i=0; i< numCliquesInCluster.size(); i++){
		seqbc<<numCliquesInCluster[i]<<" ";
		buffer[i] = numCliquesInCluster[i];
		totalCliques += numCliquesInCluster[i];
    }
    seqbcbin.write((char *)&*buffer,(numCliquesInCluster.size())*sizeof(unsigned int));
    delete[] buffer;
    seqbc.close();
    seqbcbin.close();
    //cout<<"totalCliques "<<totalCliques<<endl;
    return totalCliques;
}

void buildBcBitmap(vector<int> &positions, vector< pair< vector<int>, vector<int> > > &bicliques, set<int> &superSetS, set<int> &superSetC, ofstream &BC, set<int> &L, set<int> &M, set<int> &R){
	set<int>::iterator sit;
	vector<int>::iterator vit;
	map<int, vector<int> > bcS;
	map<int, vector<int> >::iterator mit;
	map<int, vector<int> > bcC;
	for(sit = superSetS.begin(); sit!= superSetS.end(); sit++){
		vector<int> bits;
		for(unsigned int y=0; y<positions.size(); y++){
			vector<int> bicliS = bicliques[positions[y]].first;
			vit = find(bicliS.begin(), bicliS.end(), *sit);
			if(vit == bicliS.end()){
				bits.push_back(0);
			} else {
				bits.push_back(1);
			}
		}
		bcS[*sit] = bits;
	}
	for(sit = superSetC.begin(); sit!= superSetC.end(); sit++){
		vector<int> bits;
		for(unsigned int y=0; y<positions.size(); y++){
			vector<int> bicliC = bicliques[positions[y]].second;
			vit = find(bicliC.begin(), bicliC.end(), *sit);
			if(vit == bicliC.end()){
				bits.push_back(0);
			} else {
				bits.push_back(1);
			}
		}
		bcC[*sit] = bits;
	}
	for(sit=L.begin(); sit != L.end(); sit++){
		// write only bcS in bitmapBC
		vector<int> bits = bcS[*sit];
		for(unsigned int x=0; x<bits.size(); x++){
			BC<<bits[x]<<" ";
		}
	}
	for(sit=M.begin(); sit != M.end(); sit++){
	    if(positions.size() > 1){
		// write both bcS and bcC in bitmapBC
		vector<int> bits = bcS[*sit];
		for(unsigned int x=0; x<bits.size(); x++){
			BC<<bits[x]<<" ";
		}
		bits = bcC[*sit];
		for(unsigned int x=0; x<bits.size(); x++){
			BC<<bits[x]<<" ";
		}
	    } else {
		BC<<"1 ";
	    }
	}
	for(sit=R.begin(); sit != R.end(); sit++){
		// write only bcC in bitmapBC
		vector<int> bits = bcC[*sit];
		for(unsigned int x=0; x<bits.size(); x++){
			BC<<bits[x]<<" ";
		}
	}
	//BC<<" .. ";

}
void getSetBuildHashes(vector< pair< vector<int>, vector<int> > > &bicliques, int col, int nShingles, int numSigs, string outdir){
        string seqfilename = outdir + ".seqX";
        string seqfilenamebin = outdir + ".seqX.bin";
        string seqbcfilename = outdir + ".seqBC";
        string seqbcfilenamebin = outdir + ".seqBC.bin";
	string bitmapB = outdir + ".B";
	string bitmapBc = outdir + ".Bc";
	ofstream seqx, seqxbin, seqbc, seqbcbin, B, BC;
        seqx.open(seqfilename.c_str(), std::ofstream::out);
        seqxbin.open(seqfilenamebin.c_str(), ofstream::binary);
        seqbc.open(seqbcfilename.c_str(), std::ofstream::out);
        seqbcbin.open(seqbcfilenamebin.c_str(), ofstream::binary);
        B.open(bitmapB.c_str(), std::ofstream::out);
        BC.open(bitmapBc.c_str(), std::ofstream::out);

	Sequences chosenset;
	if(col == 0){ // first
		for(unsigned int i=0; i<bicliques.size(); i++){
			chosenset.push_back(bicliques[i].first);
		}
	} else if(col == 1){ //second
		for(unsigned int i=0; i<bicliques.size(); i++){
			chosenset.push_back(bicliques[i].second);
		}
	} else {
		cerr<<" error: col must be 0 or 1"<<endl;
		exit(1);
	}
        map< vector< Shingles::Signature >, vector<int> > clustersPos;
        map< vector< Shingles::Signature >, int > clustersSizes;
        Shingles shingle(nShingles, numSigs);

        int pos = 0;

    	for (const auto& seq : chosenset) {
		vector< Shingles::Signature> multiSignatures;
		shingle.computeWSh(seq, multiSignatures, pos);
		clustersPos[multiSignatures].push_back(pos);
		clustersSizes[multiSignatures] = clustersPos[multiSignatures].size();
		pos++;
	}
        map< vector< Shingles::Signature >, vector<int> >::iterator mit;

        unsigned int *bufferBc = new unsigned int[clustersPos.size()];
	set<int>::iterator sit, rit;
	unsigned int cid=0;
	for(mit=clustersPos.begin(); mit != clustersPos.end(); mit++){
		cout<<"cluster "<<cid<<" : ";
		vector< Shingles::Signature > hashes = mit->first;
		for(unsigned int x=0; x<hashes.size(); x++){
			cout<<hashes[x]<<" ";
		}
		cout<<" pos ";
		vector<int> positions = mit->second;
		set<int> superSetS, superSetC;
		set<int> common;
		seqbc<<positions.size()<<" ";
		bufferBc[cid] = positions.size();
		for(unsigned y=0; y<positions.size(); y++){
			cout<<positions[y]<<endl;
			cout<<" Biclique "<<endl;
			vector<int> setS = bicliques[positions[y]].first;
			superSetS.insert(setS.begin(), setS.end());
			vector<int> setC = bicliques[positions[y]].second;
			superSetC.insert(setC.begin(), setC.end());
			for(unsigned z=0; z<setS.size(); z++)
				cout<<setS[z]<<" ";
			cout<<" - ";
			for(unsigned z=0; z<setC.size(); z++)
				cout<<setC[z]<<" ";
			cout<<endl;
		}
		cout<<endl;
		cout<<"superSetS "<<endl;
		for(sit=superSetS.begin(); sit!= superSetS.end(); sit++){
			cout<<*sit<<" ";
		}
		cout<<endl;
		cout<<"superSetC "<<endl;
		for(sit=superSetC.begin(); sit!= superSetC.end(); sit++){
			cout<<*sit<<" ";
		}
		cout<<endl;
		set_intersection(superSetS.begin(), superSetS.end(), superSetC.begin(), superSetC.end(), std::inserter(common, common.end()));
		set<int> L, R;
		set_difference(superSetS.begin(), superSetS.end(), common.begin(), common.end(), std::inserter(L, L.begin()));
		set_difference(superSetC.begin(), superSetC.end(), common.begin(), common.end(), std::inserter(R, R.begin()));
		cout<<" L "<<endl;
		for(sit=L.begin(); sit != L.end(); sit++)
			cout<<*sit<<" ";
		cout<<endl;
		cout<<" M "<<endl;
		for(sit=common.begin(); sit != common.end(); sit++)
			cout<<*sit<<" ";
		cout<<endl;
		cout<<" R "<<endl;
		for(sit=R.begin(); sit != R.end(); sit++)
			cout<<*sit<<" ";
		cout<<endl;
		buildBcBitmap(positions, bicliques, superSetS, superSetC, BC, L, common, R);
		unsigned int clusterseqlen = L.size()+common.size()+R.size();
        	unsigned int *buffer = new unsigned int[clusterseqlen];
		unsigned int c=0;
		B<<"1 ";
		for(sit=L.begin(); sit != L.end(); sit++){
			B<<"0 ";
			buffer[c] = *sit;
			seqx<<*sit<<" ";
			c++;
		}
		B<<"1 ";
		for(sit=common.begin(); sit != common.end(); sit++){
			B<<"0 ";
			buffer[c] = *sit;
			seqx<<*sit<<" ";
			c++;
		}
		B<<"1 ";
		for(sit=R.begin(); sit != R.end(); sit++){
			B<<"0 ";
			buffer[c] = *sit;
			seqx<<*sit<<" ";
			c++;
		}

        	seqxbin.write((char *)&*buffer,(clusterseqlen)*sizeof(unsigned int));
        	delete[] buffer;
		cid++;
	}
        seqbcbin.write((char *)&*bufferBc,(clustersPos.size())*sizeof(unsigned int));
        delete[] bufferBc;
        seqbc.close();
        seqxbin.close();
        seqx.close();
        seqbcbin.close();
	B<<"1";
	B.close();
	BC.close();
}

int
main(int argc, char **argv) {
    if (argc != 6) {
        cerr << "error: missing argument: "<<argv[0]<<" SEQUENCES_FILE nShingles nSignatures col out_dir" << endl;
	// it is optimized wrt the general biclique version. Here bicliques of size 1 has un bitmap Bc optimized
	// where M components only has 1 bit not 2
        return 1;
    }

    Sequences sequences;
    vector< pair < vector<int>, vector<int> > > biclique;
    readSequencesFromFile(argv[1], biclique);
    printBiclique(biclique);
    int nShingles = atoi(argv[2]);
    int numSigs = atoi(argv[3]);
    int col = atoi(argv[4]);
    string outdir = string(argv[5]);
    string seqfilename = outdir + ".seqX";
    string seqfilenamebin = outdir + ".seqX.bin";
    getSetBuildHashes(biclique, col, nShingles, numSigs, outdir);
    return 0;
}
