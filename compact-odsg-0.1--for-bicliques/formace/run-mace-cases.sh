#!/bin/bash


cd ./mace/ || exit 1
mkdir -p ../out/

set -x
./mace M -l 2 ../data/gavin2006-min.grf           ../out/gavin2006-min.all-cliques.txt           > ../out/gavin2006-min.all-cliques.stats 2>&1 || exit 1
./mace M -l 2 ../data/collins2007-min.grf         ../out/collins2007-min.all-cliques.txt         > ../out/collins2007-min.all-cliques.stats 2>&1 || exit 1
./mace M -l 2 ../data/biogrid-min.grf             ../out/biogrid-min.all-cliques.txt             > ../out/biogrid-min.all-cliques.stats 2>&1 || exit 1
./mace M -l 2 ../data/krogan2006_core-min.grf     ../out/krogan2006_core-min.all-cliques.txt     > ../out/krogan2006_core-min.all-cliques.stats 2>&1 || exit 1
./mace M -l 2 ../data/krogan2006_extended-min.grf ../out/krogan2006_extended-min.all-cliques.txt > ../out/krogan2006_extended-min.all-cliques.stats 2>&1 || exit 1

./mace M -l 2 ../data/dblp-2010-min.grf           ../out/dblp-2010-min.all-cliques.txt       > ../out/dblp-2010-min.all-cliques.stats 2>&1 || exit 1
./mace M -l 2 ../data/dblp-2011-min.grf           ../out/dblp-2011-min.all-cliques.txt       > ../out/dblp-2011-min.all-cliques.stats 2>&1 || exit 1

./mace M -l 2 ../data/col-min.grf           ../out/col-min.all-cliques.txt       > ../out/col-min.all-cliques.stats 2>&1 || exit 1
