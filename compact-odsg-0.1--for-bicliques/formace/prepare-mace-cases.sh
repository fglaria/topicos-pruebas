#!/bin/bash


cd ../odsg-for-bios || exit 1

set -x
./bin/printGraphForMaceInput-release graphs/gavin2006.graph.txt             > ../formace/data/gavin2006-min.grf || exit 1
./bin/printGraphForMaceInput-release graphs/collins2007.graph.txt           > ../formace/data/collins2007-min.grf || exit 1
./bin/printGraphForMaceInput-release graphs/biogrid.graph.txt               > ../formace/data/biogrid-min.grf || exit 1
./bin/printGraphForMaceInput-release graphs/krogan2006_core.graph.txt       > ../formace/data/krogan2006_core-min.grf || exit 1
./bin/printGraphForMaceInput-release graphs/krogan2006_extended.graph.txt   > ../formace/data/krogan2006_extended-min.grf || exit 1

./bin/printGraphForMaceInput-release ../odsg-for-social-web/graphs/dblp-2010.graph.txt   > ../formace/data/dblp-2010-min.grf || exit 1
./bin/printGraphForMaceInput-release ../odsg-for-social-web/graphs/dblp-2011.graph.txt   > ../formace/data/dblp-2011-min.grf || exit 1
