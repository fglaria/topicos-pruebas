#include "DagVolatileForest.hpp"

#include <cassert>      // Support run-time assertions. They can be disabled defining the NDEBUG macro
#include <stdexcept>
#include <memory>       // std::auto_ptr

#include "Graph.hpp"
#include "GraphPartitioner.hpp"

namespace odsg {


DagVolatileForest::DagVolatileForest(const Graph* g,
                                     int clusteringScheme,
                                     unsigned int minClusterSize,
                                     bool sortClusterByFrequency)
: graph(g), partition(), clusterFrequencySorting(sortClusterByFrequency) {

    assert(g);
    assert(clusteringScheme == 1 || clusteringScheme == 2);     // No unified forests (forbidden for huge graphs)

    if (!graph->isMineable()) {
        throw std::logic_error("DagVolatileForest::DagVolatileForest(): graph must be mineable");
    }

    GraphPartitioner* partitionerPtr = NULL;
    switch (clusteringScheme) {
        case 1:     // Partitioning by common initial outlink
            partitionerPtr = new GraphPartitionerByInitialOutlink(graph);
            break;
        case 2:     // Partitioning by common hashing signature
            partitionerPtr = new GraphPartitionerBySignature(graph);
            break;
    }
    std::auto_ptr<GraphPartitioner> partitioner(partitionerPtr);    // It takes care of delete the object

    GraphCluster cluster = partitioner->next(minClusterSize);
    while (!cluster.empty()) {
        partition.push_back(cluster);
        cluster = partitioner->next(minClusterSize);
    }

    nextCluster = partition.begin();
}


const Dag*
DagVolatileForest::next() {     // TODO: Return auto_ptr instead of raw pointer
    if (nextCluster == partition.end())
        return NULL;

    assert(graph);

    const Dag* dag;
    if (clusterFrequencySorting) {
        Graph clusterGraph(*nextCluster);   // It create a copy of the data
        clusterGraph.rebuildForMiningExceptSorting();   // Required by VertexFrequencyComparer
        clusterGraph.rebuildForMining(Graph::VertexFrequencyComparer(clusterGraph));

        dag = new Dag(clusterGraph);
    } else {
        dag = new Dag(*nextCluster, graph->isSortedByVertex());
    }
    ++nextCluster;

    return dag;
}


void
DagVolatileForest::reset() {
    nextCluster = partition.begin();
}


}   // namespace odsg
