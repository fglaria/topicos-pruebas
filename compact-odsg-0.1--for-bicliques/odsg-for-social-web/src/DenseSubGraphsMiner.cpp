#include "DenseSubGraphsMiner.hpp"

#include <cassert>      // Support run-time assertions. They can be disabled defining the NDEBUG macro
#include <cstddef>      // NULL

#include "utils/algorithms.hpp"
#include "Dag.hpp"
#include "DagNode.hpp"
#include "DenseSubGraph.hpp"
#include "DenseSubGraphsMaximalSet.hpp"
#include "MinerDagTraveler.hpp"
#include "MinerObjective.hpp"

namespace odsg {


DenseSubGraphsMiner::DenseSubGraphsMiner(const Dag* d,
                                         MinerDagTraveler* traveler,
                                         MinerObjective* objective,
                                         bool asCliques,
                                         unsigned long arcsCount,
                                         bool noCompressibles)
: dag(d), parentTraveler(traveler), minerObjective(objective),
  asCliquesOnly(asCliques), minArcsCount(arcsCount), includeNoCompressibles(noCompressibles) {

    assert(d != NULL);
    assert(traveler != NULL);
    assert(objective != NULL);
    assert(!asCliques || dynamic_cast<AsCliqueMinerObjective*>(objective));     // Use proper objective for cliques

    // Set the traveling paths inside the DagNodes objects for a faster retrieval later, during mining.
    // WARN: The implementation is hacky, as it was added in final stages of development, breaking in the practice
    // the const property of the dag. Note that it relies in no other DenseSubGraphsMiner instances being built for
    // the same dag until that the mining entrusted to the first instance is done.
    for (Dag::const_iterator it = dag->begin(); it != dag->end(); ++it) {
        const DagNode* node = *it;

        node->setNextTravelingNode(traveler->next(node));

        assert(node->getNextTravelingNode() != NULL || node->isRoot());
    }
}


DenseSubGraphsMaximalSet
DenseSubGraphsMiner::mine() const {
    DenseSubGraphsMaximalSet dagDSGs(asCliquesOnly);

    // The next pragma directives support parallel execution when the project is compiled to exploit it.
    // Otherwise, the pragmas are ignored and the work is done secuencially.
    //
    // The use of 'guided' as scheduler exploits the fact that the mining starting from latter nodes (mostly leafs)
    // is slower.
    #pragma omp parallel for schedule(guided)
        for (Dag::const_iterator it = dag->begin(); it < dag->end(); ++it) {    // The use of '<' is needed by openMP
            const DagNode* const node = *it;

            if (willNotProvideEnoughGoodDsg(node))
                continue;

            // The next will find one big dense subgraph that includes to the current node in its centers set
            DenseSubGraph nodeDsg = getDenseSubGraphFrom(node);

            if (!isGoodEnough(nodeDsg))
                continue;

            // Try to insert the dense subgraph in the ongoing collection; if it is not maximal it will be ignored.
            #pragma omp critical
                dagDSGs.insert(nodeDsg);

        }   // All threads join here to the master thread and terminate

    assert(dagDSGs.size() <= dag->nodesCount());
    return dagDSGs;
}


bool
DenseSubGraphsMiner::willNotProvideEnoughGoodDsg(const DagNode* node) const {
    assert(node != NULL);
    assert(!node->getVertexes().empty());

    bool notProvideGood = false;
    if (!includeNoCompressibles) {
        // These cases will provide a dense subgraph with at least one of their components with size == 1;
        // they are not useful for our compression purposes, but they can still be mined if desired.
        notProvideGood = node->isRoot() || node->getVertexes().size() == 1;
    }

    // These cases will not provide a dense subgraph with the enough number of arcs.
    // Note that a stricter condition could be defined, for example using the length of the mining path (dependent
    // of the MinerDagTraveler object in use), instead of using a upper bound for it, as it's node->getMaxDepth().
    // That would help with big values of minArcsCount, for smaller values the extra computation can not be worth.
    bool notProvideEnoughBig = false;
    if (asCliquesOnly)
        notProvideEnoughBig = (node->getMaxDepth() * node->getMaxDepth()) < minArcsCount;
    else
        notProvideEnoughBig = (node->getVertexes().size() * node->getMaxDepth()) < minArcsCount;

    return notProvideGood || notProvideEnoughBig;
}


DenseSubGraph
DenseSubGraphsMiner::getDenseSubGraphFrom(const DagNode* node) const {
    assert(node != NULL);

    DenseSubGraph nodeDsg(node->getVertexes(), node->label);

    // For each node in the path, we check if adding it to the centers set provides us a 'better' dense subgraph
    const DagNode* pathNode = node;
    while ((pathNode = pathNode->getNextTravelingNode())) {     // Yes, it's an assignment
        DenseSubGraph candidateDsg(pathNode->getVertexes(), pathNode->label);
        candidateDsg.merge(nodeDsg);

        if (minerObjective->better(nodeDsg, candidateDsg)) {
            nodeDsg.swap(candidateDsg);

            if (minerObjective->best(nodeDsg))
                break;
        }
    }

    assert(algorithms::is_found(nodeDsg.getCenters(), node->label));
    assert(nodeDsg.getCenters().size() <= node->getMaxDepth());
    assert(nodeDsg.getSources().size() <= node->getVertexes().size());

    return nodeDsg;
}


bool
DenseSubGraphsMiner::isGoodEnough(const DenseSubGraph& dsg) const {
    assert(!dsg.getCenters().empty());
    assert(!dsg.getSources().empty());

    bool isBigEnough = dsg.arcsCount(asCliquesOnly) >= minArcsCount;
    bool isCompressible = dsg.getCenters().size() > 1 && dsg.getSources().size() > 1;

    return isBigEnough && (isCompressible || includeNoCompressibles);
}


}   // namespace odsg
