#include "Shingles.hpp"

#include <cassert>      // Support run-time assertions. They can be disabled defining the NDEBUG macro
#include <cstddef>      // NULL, std::size_t
#include <string>
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time

#include <boost/functional/hash.hpp>

#include "utils/algorithms.hpp"
#include "utils/strings.hpp"

namespace odsg {


Shingles::Shingles() {
    std::srand(std::time(NULL));
    A = (std::rand() % bigPrime) + 1;
    B = (std::rand() % bigPrime) + 1;
}


Shingles::Signature
Shingles::sign(const Graph::AdjacencyList& outlinks) const {
    return sign_(outlinks);
}


template<typename ContainerT>
Shingles::Signature
Shingles::sign_(const ContainerT& outlinks) const {
    assert(!outlinks.empty());
    assert(algorithms::has_unique(outlinks));

    boost::hash<std::string> stringHash;
    Signature minShingleHash = bigPrime;

    for (typename ContainerT::const_iterator vxit = outlinks.begin(); vxit != outlinks.end(); ++vxit) {
        std::size_t shingleID = stringHash(strings::to_str(*vxit));

        // TODO: I need to re-examine these calculations to manage 32/64 bits support
        Signature shingleHash = (((unsigned long) A * (unsigned long) shingleID) + B) % bigPrime;
        if (minShingleHash > shingleHash) {
            minShingleHash = shingleHash;
        }
    }
    return minShingleHash;
}


}   // namespace odsg
