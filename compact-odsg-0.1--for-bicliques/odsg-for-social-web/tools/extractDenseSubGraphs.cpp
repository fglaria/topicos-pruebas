#include <exception>
#include <string>
#include <vector>
#include <iostream>
#include <memory>       // std::auto_ptr

#include <boost/format.hpp>
#include <tclap/CmdLine.h>

#include <utils/strings.hpp>
#include <DagVolatileForest.hpp>
#include <DenseSubGraphsMaximalSet.hpp>
#include <Graph.hpp>

using namespace odsg;


struct CmdLineArgs {    // Check definition of processCmdLine() to get a description of each option
    std::string graphFileName;
    std::string outputFilesPath;
    bool comeSorted;
    unsigned int partitioning;
    unsigned int minClusterSize;
    bool clusterSorting;
    unsigned int minerTraveler;
    unsigned int minerObjective;
    bool asCliques;
    std::vector<unsigned int> minSizes;
    unsigned int threshold;
};

CmdLineArgs processCmdLine(int argc, char* argv[]);


/*
 * Iterative mining and extraction of dense subgraphs from big social/web graphs.
 */
int
main(int argc, char* argv[]) {

    CmdLineArgs args;
    try {
        args = processCmdLine(argc, argv);
    } catch (TCLAP::ArgException& e) {
        std::cerr << "error: " << e.argId() << '\n'
                  << "       " << e.error() << std::endl;
        return 1;
    }


    // Build the graph object from the input file /////////////////////////////////////////////////////////////////
    Graph myGraph;
    std::cout << "Building initial graph... " << std::flush;
    try {
        myGraph = Graph(args.graphFileName, args.comeSorted);
    } catch (std::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        return 1;
    }
    std::cout << "OK\n" << std::endl;


    // The mining/extracting process is repeated for each specified minimum size while the total number of
    // mined dense subgraphs results be above the specified threshold (and the graph doesn't result be empty).
    std::vector<unsigned int>::const_iterator minSizesIterator = args.minSizes.begin();
    unsigned int iterationCount = 0;

    while (minSizesIterator != args.minSizes.end()) {
        unsigned int currentMinSize = *minSizesIterator;
        ++iterationCount;

        std::cout << "Iteration " << iterationCount
                  << ", with a minimum size for the dense subgraphs to be mined = " << currentMinSize
                  << std::endl;


        //std::string currentOutputFileNameBase = boost::str(boost::format("it-%04d.minSize-%d")
         //                                                   % iterationCount % currentMinSize);
        std::string currentOutputFileNameBase = boost::str(boost::format("it-%d") % iterationCount);
        std::string dsgsFileNameBase = currentOutputFileNameBase + ".mined.txt";
        std::string remainingGraphFileNameBase = currentOutputFileNameBase + ".remaining-graph.txt";
        std::string dsgsFileName = args.outputFilesPath + "/" + dsgsFileNameBase;
        std::string remainingGraphFileName = args.outputFilesPath + "/" + remainingGraphFileNameBase;


        // Rebuild the graph for mining ///////////////////////////////////////////////////////////////////////////
        std::cout << "Rebuilding graph for mining... " << std::flush;
        if (args.comeSorted)
            myGraph.rebuildForMining();     // Default implementation will skip internally the sorting
        else
            myGraph.rebuildForMining(Graph::VertexComparer());  // Not use the default sorting by frequency: too slow

        if (myGraph.empty()) {
            std::cout << "the resultant graph is empty!" << std::endl;

            break;
        }
        std::cout << "OK" << std::endl;


        // Initialize the forest //////////////////////////////////////////////////////////////////////////////////
        std::cout << "Initializating the dag forest... " << std::flush;
        DagVolatileForest myForest(&myGraph, args.partitioning, args.minClusterSize, args.clusterSorting);
        std::cout << "OK" << std::endl;
        std::cout << "  " << myForest.size() << " dags will be generated"
                                             << (iterationCount == 1? " at the first iteration" : "") << std::endl;


        // Extract from graph the mined dense subgraphs; dump them to a file //////////////////////////////////////
        std::cout << "Mining dense subgraphs, extracting them from graph and dumping them to file... " << std::flush;

        unsigned long dagsCount = 0;
        unsigned long minedDsgsCount = 0;
        const Dag* myDagPtr;

        while ((myDagPtr = myForest.next())) {
            dagsCount++;
            std::auto_ptr<const Dag> myDag(myDagPtr);     // It takes care of delete the object

            DenseSubGraphsMaximalSet myDagDSGs = myDag->getDenseSubGraphs(args.minerTraveler,
                                                                          args.minerObjective,
                                                                          args.asCliques,
                                                                          currentMinSize,
                                                                          false /*=includeNoCompressibles*/);
            minedDsgsCount += myDagDSGs.size();
            if (dagsCount == 1 || !myDagDSGs.empty()) {     // Prevent multiple blank spaces in dsgsFileName
                try {
                    myDagDSGs.dump(dsgsFileName,
                                   /*append =*/ dagsCount != 1,
                                   /*descriptions =*/ false);
                } catch (std::exception& e) {
                    std::cerr << "error: " << e.what() << std::endl;
                    return 1;
                }
            }

            myGraph.extract(myDagDSGs);
        }
        std::cout << "OK" << std::endl;
        std::cout << "  " << minedDsgsCount << " dense subgraphs was mined and extracted" << std::endl;
        std::cout << "    Dumped mined dense subgraphs to file '" << dsgsFileNameBase << "'" << std::endl;


        // End iteration //////////////////////////////////////////////////////////////////////////////////////////
        if (minedDsgsCount <= args.threshold) {
            std::cout << "Amount of mined dense subgraphs isn't above the specified threshold = " << args.threshold
                      << ": moving to the next given minimum size" << std::endl;

            // Dump the remaining graph to a file
            std::cout << "  Dumping remaining graph to file '" << remainingGraphFileNameBase << "'... " << std::flush;
            try {
                myGraph.dump(remainingGraphFileName, /*extraSummary =*/ true);
            } catch (std::exception& e) {
                std::cerr << "error: " << e.what() << std::endl;
                return 1;
            }
            std::cout << "OK" << std::endl;

            ++minSizesIterator;
        }
        std::cout << std::endl;
    }
    std::cout << "No more iterations" << std::endl;
    std::cout << iterationCount << " iterations run at all" << std::endl;

    return 0;
}



/*
 * The next does use of the Templatized C++ Command Line Parser (TCLAP) library, in include/ directory.
 *   http://tclap.sourceforge.net/manual.html
 */
CmdLineArgs
processCmdLine(int argc, char* argv[]) {

    //// Define the main command line object //////////////////////////////////////////////////////////////////////
    TCLAP::CmdLine cmd("Mine and extract dense subgraphs from social/web graphs using a DAG-based approach"
                            " -- Carlos Mella, Cecilia Hernandez",
                                    // Message to be displayed in the USAGE output
                       ' ',         // Character used to separate the argument flag/name from the value
                       "1",         // Version number to be displayed by the --version switch
                       false);      // Whether or not to create the automatic --help and --version switches


    //// Arguments are separate objects, added to the CmdLine object one at a time ////////////////////////////////

    // Switch args are boolean arguments to define a flag; its presence negates its default value
    TCLAP::SwitchArg notComeSortedArg(
        "n",
        "not-came-sorted",
        "Indicate that the input graph does NOT have its adjacency lists sorted by increasing id,"
            " requiring then do it in the first place. Slow.",
        cmd,
        false);
    TCLAP::SwitchArg clusterSortingArg(
        "l",
        "local-cluster-frequency-sorting",
        "Sort (again) the adjacency lists of each merged cluster (option -z), this time by local apparition frequency."
            " It's ignored if the -p 0 option is used.",
        cmd,
        false);
    TCLAP::SwitchArg asCliquesArg(
        "c",
        "as-cliques",
        "Limit the mining to dense subgraphs with maximal centers sets between themselves, i.e. cliques."
            " Require be used in conjuntion with -j 0 option. Not really compatible with filtering by minimum size.",
        cmd,
        false);

    // Value args defines a flag and a type of value that it expects
    TCLAP::ValueArg<unsigned int> partitioningArg(
        "p",
        "partitioning-scheme",
        "Set how the adjacency lists of the input graph are partitioned in 'clusters', from which each dag is built:"
            " 1 groups adjacency lists with the same first outlink after sorting them;"
            " 2 groups adjacency lists with the same 'shingling' signature."
            " Defaults to 2.",
        false,
        1,
        "INTEGER",
        cmd);
    TCLAP::ValueArg<unsigned int> minClusterSizeArg(
        "z",
        "min-merged-cluster-size",
        "It withholds any cluster generated by the partitioning with a size, defined as the number of arcs, below"
            " the specified value, and merge it with the next until the combined size of all of them exceed it."
            " Defaults to 0 (no merge at all); ignored with -p 0 option.",
        false,
        0,
        "INTEGER",
        cmd);
    TCLAP::ValueArg<unsigned int> minerTravelerArg(
        "t",
        "mining-traveler",
        "Set how to travel from each node to another in the dag, while mining dense subgraphs:"
            " 0 move to the deepest parent, trying to maximize the length of the mining path;"
            " 1 move to the parent with whom it shares the largest number of vertexes."
            " Defaults to 0.",
        false,
        0,
        "INTEGER",
        cmd);
    TCLAP::ValueArg<unsigned int> minerObjectiveArg(
        "j",
        "dense-subgraphs-mining-objetive",
        "Objective function to try to maximize for the dense subgraphs being mined:"
            " 0 maximizes the size of the centers while it is kept as a subset of the sources,"
                " favoring the mining of cliques (see -c option too);"
            " 1 maximizes the number of arcs (same as the legacy implementation from chernand);"
            " 2 maximizes the number of shared elements between sources and centers."
            " Defaults to 0.",
        false,
        0,
        "INTEGER",
        cmd);

    // Unlabeled value args aren't identified by a flag, instead they are identified by their position in the argv
    // array. Note that:
    //  - the order that they are added here to the cmd object is the order that they will be parsed.
    //  - only one optional UnlabeledValueArg is possible; it must be the last listed.
    TCLAP::UnlabeledValueArg<std::string> graphFileNameArg(
        "GRAPH_FILE",
        "Path to a input text file defining a graph...",
        true,
        "",
        "GRAPH_FILE",
        cmd);
    TCLAP::UnlabeledValueArg<std::string> outputFilesPathArg(
        "OUTPUT_PATH",
        "Path and name of the folder containing the text files generated as output.",
        true,
        "",
        "OUTPUT_PATH",
        cmd);
    TCLAP::UnlabeledValueArg<std::string> minSizesArg(
        "MINIMUM_SIZES",
        "Comma-separated list of decreasing minimum sizes, defined as the number of arcs,"
            "for the mined dense subgraphs.",
        true,
        "",
        "MINIMUM_SIZES",
        cmd);
    TCLAP::UnlabeledValueArg<unsigned int> thresholdArg(
        "THRESHOLD",
        "... ; defaults to 2.",
        true,
        2,
        "THRESHOLD",
        cmd);


    //// Parse the argv array /////////////////////////////////////////////////////////////////////////////////////
    cmd.parse(argc, argv);

    // Extra validation checks
    if (graphFileNameArg.getValue().empty())
        throw TCLAP::CmdLineParseException("Empty argument!", graphFileNameArg.longID());

    if (outputFilesPathArg.getValue().empty())
        throw TCLAP::CmdLineParseException("Empty argument!", outputFilesPathArg.longID());

    if (partitioningArg.getValue() == 0 || partitioningArg.getValue() > 2)
        throw TCLAP::CmdLineParseException("Value out of range!", partitioningArg.longID());

    if (minerTravelerArg.getValue() > 1)
        throw TCLAP::CmdLineParseException("Value out of range!", minerTravelerArg.longID());

    if (minerObjectiveArg.getValue() > 2)
        throw TCLAP::CmdLineParseException("Value out of range!", minerObjectiveArg.longID());

    if (asCliquesArg.getValue() && minerObjectiveArg.getValue() != 0)
        throw TCLAP::CmdLineParseException("Illegal combination of values for these options!",
                                           minerObjectiveArg.longID() + "; " + asCliquesArg.longID());

    std::vector<unsigned int> minSizes;
    if (minSizesArg.getValue().empty())
        throw TCLAP::CmdLineParseException("Empty argument!", minSizesArg.longID());
    try {
        minSizes = strings::split<unsigned int>(minSizesArg.getValue());
    } catch (std::exception& e) {
        throw TCLAP::ArgParseException("Parsing error!", minSizesArg.longID());
    }


    //// Get the value parsed by each argument ////////////////////////////////////////////////////////////////////
    CmdLineArgs args;

    args.graphFileName = graphFileNameArg.getValue();
    args.outputFilesPath = outputFilesPathArg.getValue();
    args.comeSorted = not notComeSortedArg.getValue();
    args.partitioning = partitioningArg.getValue();
    args.minClusterSize = minClusterSizeArg.getValue();
    args.clusterSorting = clusterSortingArg.getValue();
    args.minerTraveler = minerTravelerArg.getValue();
    args.minerObjective = minerObjectiveArg.getValue();
    args.asCliques = asCliquesArg.getValue();
    args.threshold = thresholdArg.getValue();
    args.minSizes.swap(minSizes);

    return args;
}
