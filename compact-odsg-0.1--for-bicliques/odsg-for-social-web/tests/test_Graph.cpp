#include <Graph.hpp>

#include <vector>
#include <map>
#include <set>

#include <catch.hpp>

#include <Vertex.hpp>
#include <DenseSubGraphsMaximalSet.hpp>

using namespace odsg;


// The next are basic common data examples used by most of the test cases.
typedef std::map<Vertex, std::vector<Vertex>> AsGraphMap;

const AsGraphMap emptyMap{};
const AsGraphMap onlySelfLoopsMap{ {1, {1}}, {2, {2}}, {5, {5}}, {6, {6}} };
const AsGraphMap noOutlinksMap{ {9, {}}, {8, {}}, {7, {}} };
const AsGraphMap K3WithSelfLoopsMap{ {1, {1, 2, 3}}, {2, {1, 2, 3}}, {3, {1, 2, 3}} };


TEST_CASE("Graph::Graph", "[graph]") {
    Graph empty;
    CHECK(empty.empty());

    Graph empty3(::emptyMap);
    CHECK(empty3.empty());

    const std::map<Vertex, std::set<Vertex>> emptyMap2;
    Graph empty4(emptyMap2);
    CHECK(empty4.empty());

}


TEST_CASE("Graph::rebuildForMining", "[graph]") {
    Graph empty;
    empty.rebuildForMining();
    CHECK(empty.empty());

}


TEST_CASE("Graph::isMineable", "[graph]") {
    Graph empty;
    CHECK(empty.isMineable());

}


TEST_CASE("Graph::extract", "[graph]") {
    Graph mineable;
    mineable.extract(DenseSubGraphsMaximalSet());
    CHECK(mineable.isMineable());
    mineable.extract(DenseSubGraph({0, 1}, {0, 1, 2}));
    CHECK(mineable.isMineable());

    const AsGraphMap denseMap{ {1, {3, 4}}, {3, {3, 4}} };
    Graph dense(denseMap);
    dense.extract(DenseSubGraph({1, 3}, {3, 4}));
    CHECK(dense.arcsCount() == 0);
    CHECK(dense.nodesCount() == dense.listsCount());
    CHECK_FALSE(dense.isMineable());

    // Pending: optional asClique option
}


TEST_CASE("Graph::listsCount", "[graph]") {
    Graph empty;
    CHECK(empty.listsCount() == 0);

    Graph empty2(::emptyMap);
    CHECK(empty2.listsCount() == 0);

}


TEST_CASE("Graph::arcsCount", "[graph]") {
    Graph empty;
    CHECK(empty.arcsCount() == 0);

    Graph empty2(::emptyMap);
    CHECK(empty2.arcsCount() == 0);

    Graph noOutlinks(::noOutlinksMap);
    CHECK(noOutlinks.arcsCount() == 0);

    Graph onlySelfLoops(::onlySelfLoopsMap);
    CHECK(onlySelfLoops.arcsCount() == ::onlySelfLoopsMap.size());

    Graph K3WithSelfLoops(::K3WithSelfLoopsMap);
    CHECK(K3WithSelfLoops.arcsCount() == ::K3WithSelfLoopsMap.size() * ::K3WithSelfLoopsMap.size());

}


TEST_CASE("Graph::nodesCount", "[graph]") {
    Graph empty;
    CHECK(empty.nodesCount() == 0);

    Graph empty2(::emptyMap);
    CHECK(empty2.nodesCount() == 0);

    Graph noOutlinks(::noOutlinksMap);
    CHECK(noOutlinks.nodesCount() == ::noOutlinksMap.size());

    Graph onlySelfLoops(::onlySelfLoopsMap);
    CHECK(onlySelfLoops.nodesCount() == ::onlySelfLoopsMap.size());

    Graph K3WithSelfLoops(::K3WithSelfLoopsMap);
    CHECK(K3WithSelfLoops.nodesCount() == ::K3WithSelfLoopsMap.size());

    const AsGraphMap map1{ {1, {2}}, {3, {3, 7, 8}}, {4, {}}, {5, {5, 6, 11, 12}}, {6, {5, 6, 11, 13}} };
    Graph graph1(map1);
    CHECK(graph1.nodesCount() == 11);
}
