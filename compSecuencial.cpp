// g++ -std=c++11 compSecuencial.cpp solution.cpp -o compSecuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <set>
#include <map>
#include <string>
#include <sys/times.h>
#include <chrono>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "solution.h"


void secuencial(sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm, sdsl::rrr_vector<63> &rrrB1,
    sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm, sdsl::wt_hutu<sdsl::rrr_vector<63>> &b4_wt,
    std::map<uint32_t, std::set<uint32_t>> &allNeighbors)
{
    // SDSL variables for B1
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

    uint32_t currentY = 0;
    uint32_t nextY = y3_wm[0];
    uint32_t currentB1Index = 0;
    uint32_t nextB1Index = rrrB1_sel(1);

    // How many partitions for this graph
    const uint32_t howManyPartitions = rrrB1_rank(rrrB1.size());

    // For every partition, let's find neighbors
    for (uint32_t partition = 1; partition < howManyPartitions; ++partition)
    {
        // Update B1 indexes, and count elements in partition
        currentB1Index = nextB1Index;
        nextB1Index = rrrB1_sel(partition + 1);
        const uint32_t elements = nextB1Index - currentB1Index;

        // Update Y values, and get bytes on partition and per element
        currentY = nextY;
        nextY = y3_wm[partition];
        const uint32_t bytesInB4 = nextY - currentY;
        const uint32_t bytesPerMask = bytesInB4/elements;

        // Temporary array for storing elements in partition
        uint32_t arrayX[elements];

        // Copy every element to temporary arrayX
        for (uint32_t iElement = 0; iElement < elements; ++iElement)
        {
            arrayX[iElement] = x_wm[currentB1Index + iElement];
        }

        unsigned char arrayB4[bytesInB4];
        for (int i = 0; i < bytesInB4; ++i)
        {
            arrayB4[i] = b4_wt[currentY + i];
        }

        // For every element in partition, let's get their neighbors
        for (uint32_t iElement = 0; iElement < elements; ++iElement)
        {
            // If just one byte on B4, it means they're all neighbors
            // (should be one bit per element; new format translate this to one byte per partition)
            if(1 == bytesInB4)
            {
                for(uint32_t mm = 0; mm < elements; ++mm)
                {
                    allNeighbors[arrayX[mm]].insert(arrayX, arrayX + elements);
                }
            }
            else
            {
                // Load current element to find neighbors
                uint32_t currentX = arrayX[iElement];

                // If more than one byte per partition, let's search for some neighbors
                for(uint32_t iNextElement = iElement+1; iNextElement < elements; ++iNextElement)
                {
                    const uint32_t possibleX = arrayX[iNextElement];
                    // if (currentX == possibleX)
                    // {
                    //     continue;
                    // }

                    // Count how many bytes are checked, and store possible neighbor
                    uint32_t bytesChecked = 0;

                    // While there are byte to check
                    while (bytesChecked != bytesPerMask)
                    {
                        // Get byte mask to check neighbor
                        const unsigned char maskOfSearchedNode = arrayB4[iElement*bytesPerMask + bytesChecked];
                        const unsigned char maskOfPossibleNeighbor = arrayB4[iNextElement*bytesPerMask + bytesChecked];

                        // Compare masks
                        const unsigned char maskResult = maskOfPossibleNeighbor & maskOfSearchedNode;

                        // If result is anything but zero, they're neighbors
                        // List them, then break while
                        if (0 != maskResult)
                        {
                            allNeighbors[currentX].insert(possibleX);
                            allNeighbors[possibleX].insert(currentX);

                            break;
                        }

                        // Next byte to copy mask
                        bytesChecked += 1;
                    }
                }
            }
        }
    }
}



int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Iterations is 20, unless it's given
    const uint32_t iterations = argv[2] ? atoi(argv[2]) : 20;


    // Time to acumulate
    double t_acum = 0.0;

    sdsl::wm_int<sdsl::rrr_vector<63>> x_wm;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wt_hutu<sdsl::rrr_vector<63>> b4_wt;
    sdsl::wm_int<sdsl::rrr_vector<63>> y3_wm;

    solution::readDataB4(path, x_wm, rrrB1, b4_wt, y3_wm);


    // Generate original graph iterations times to have an average
    for (int i = 1; i <= iterations; ++i)
    {
        // Get all nodes of graph
        std::map<uint32_t, std::set<uint32_t>> allGraph;

        std::chrono::high_resolution_clock::time_point st = std::chrono::high_resolution_clock::now();

        secuencial(x_wm, rrrB1, y3_wm, b4_wt, allGraph);

        std::chrono::high_resolution_clock::time_point et = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds> (et-st).count();


        std::ofstream allNodesFile(path + ".allNodesCS", std::ofstream::out | std::ofstream::trunc);


        uint32_t alledges = 0;
        // Write nodes to file
        for (auto mapped : allGraph)
        {
            allNodesFile << mapped.first << ": ";
            for (auto neighbor : mapped.second)
            {
                allNodesFile << neighbor << " ";

                alledges += (mapped.first != neighbor) ? 1 : 0;
            }
            allNodesFile << std::endl;
        }

        allNodesFile.close();


        t_acum += duration;

        std::cout << "Loop " << i << " " << solution::time_representation(duration) << " Alledges " << alledges << std::endl;
    }

    double average = t_acum/iterations;

    std::cout << "Promedio " << solution::time_representation(average) << std::endl;

    return 0;
}
