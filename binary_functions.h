#ifndef TOPICOS_BINARY
#define TOPICOS_BINARY

#include <sstream>

namespace binary
{
    std::string print(unsigned short c, unsigned int y=8, bool separate=false);
}

#endif