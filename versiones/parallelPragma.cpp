// // g++ -std=c++11 parallel.cpp solution.cpp -o parallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG
// // binary_functions.cpp (add if binary library)

// #include <set>
// #include <map>
// #include <string>
// #include <sys/times.h>
// #include <chrono>
// #include "vectorclass/vectorclass.h"

// #include <sdsl/int_vector.hpp>
// #include <sdsl/bit_vectors.hpp>
// #include <sdsl/util.hpp>
// #include <sdsl/rank_support.hpp>
// #include <sdsl/select_support.hpp>
// #include <sdsl/suffix_arrays.hpp>
// #include <sdsl/suffix_arrays.hpp>

// #include "solution.h"


// void parallelPragma(sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt, sdsl::rrr_vector<63> &rrrB1,
//     sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt, unsigned char *B4,
//     std::map<uint32_t, std::set<uint32_t>> &allNeighbors)
// {
//     // SDSL variables for B1
//     sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
//     sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

//     uint32_t currentY = 0;
//     uint32_t nextY = y3_wt[0];
//     uint32_t currentB1Index = 0;
//     uint32_t nextB1Index = rrrB1_sel(1);

//     // How many partitions for this graph
//     const uint32_t howManyPartitions = rrrB1_rank(rrrB1.size());

//     // Define and initialize unsigned char vectors for masks
//     Vec16uc masksPossibleNeighbors = 0;
//     Vec16uc masksSearchedNode = 0;

//     // Max size of vectors
//     const uint32_t maxSizeVector = sizeof(masksPossibleNeighbors);

//     // Number of masks in Vectors
//     uint8_t howManyMasksInVectors = 0;

//     // Array of current X & compared
//     uint32_t currentX[maxSizeVector];
//     uint32_t comparedX[maxSizeVector];

//     // For every partition, let's find neighbors
//     #pragma omp parallel shared(allNeighbors) private(allN)
//     {
//         #pragma omp for
//         for (uint32_t partition = 1; partition < howManyPartitions; ++partition)
//         {
//             // Update B1 indexes, and count elements in partition
//             currentB1Index = nextB1Index;
//             nextB1Index = rrrB1_sel(partition + 1);
//             const uint32_t elements = nextB1Index - currentB1Index;

//             // Update Y values, and get bytes on partition and per element
//             currentY = nextY;
//             nextY = y3_wt[partition];
//             const uint32_t bytesInB4 = nextY - currentY;
//             const uint32_t bytesPerMask = bytesInB4/elements;

//             // Temporary array for storing elements in partition
//             uint32_t arrayX[elements];

//             // Copy every element to temporary arrayX
//             for (uint32_t iElement = 0; iElement < elements; ++iElement)
//             {
//                 arrayX[iElement] = x_wt[currentB1Index + iElement];
//             }

//             // For every element in partition, let's get their neighbors
//             for (uint32_t iElement = 0; iElement < elements; ++iElement)
//             {
//                 // If just one byte on B4, it means they're all neighbors
//                 // (should be one bit per element; new format translate this to one byte per partition)
//                 if(1 == bytesInB4)
//                 {
//                     for(uint32_t mm = 0; mm < elements; ++mm)
//                     {
//                         allNeighbors[arrayX[mm]].insert(arrayX, arrayX + elements);
//                     }
//                 }
//                 else
//                 {
//                     // If more than one byte per partition, let's search for some neighbors
//                     for(uint32_t iNextElement = iElement+1; iNextElement < elements; ++iNextElement)
//                     {
//                         // Count how many bytes are checked, and store possible neighbor
//                         uint32_t bytesChecked = 0;
//                         const uint32_t possibleX = arrayX[iNextElement];

//                         // While there are byte to check
//                         while (bytesChecked != bytesPerMask)
//                         {

//                             // Get byte mask to check neighbor
//                             const unsigned char maskOfSearchedNode = B4[currentY + iElement*bytesPerMask + bytesChecked];
//                             const unsigned char maskOfPossibleNeighbor = B4[currentY + iNextElement*bytesPerMask + bytesChecked];

//                             // Put masks on vectors
//                             masksPossibleNeighbors.insert(howManyMasksInVectors, maskOfPossibleNeighbor);
//                             masksSearchedNode.insert(howManyMasksInVectors, maskOfSearchedNode);

//                             // Store X value
//                             currentX[howManyMasksInVectors] = arrayX[iElement];
//                             comparedX[howManyMasksInVectors] = arrayX[iNextElement];

//                             // Plus one to how many bytes are in vectors
//                             howManyMasksInVectors += 1;

//                             // If vectors are full
//                             if (maxSizeVector == howManyMasksInVectors)
//                             {
//                                 solution::evaluateNeighbors(masksPossibleNeighbors, masksSearchedNode,
//                                     currentX, comparedX, allNeighbors);

//                                 // Clean vectors
//                                 masksPossibleNeighbors = 0;
//                                 masksSearchedNode = 0;

//                                 howManyMasksInVectors = 0;
//                             }

//                             // Next byte to copy mask
//                             bytesChecked += 1;
//                         }
//                     }
//                 }
//             }
//         }

//         solution::evaluateNeighbors(masksPossibleNeighbors, masksSearchedNode,
//             currentX, comparedX, allNeighbors);
//     }
// }



// int main(int argc, char* argv[]) {

//     if(2 > argc)
//     {
//         std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
//         return -1;
//     }

//     // Path to files
//     const std::string path(argv[1]);

//     // Iterations is 20, unless it's given
//     const uint32_t iterations = argv[2] ? atoi(argv[2]) : 20;


//     // Time to acumulate
//     double t_acum = 0.0;

//     sdsl::wm_int<sdsl::rrr_vector<63>> x_wt;
//     sdsl::rrr_vector<63> rrrB1;
//     sdsl::wm_int<sdsl::rrr_vector<63>> y3_wt;
//     unsigned char *B4 = solution::readData(path, x_wt, rrrB1, y3_wt);


//     // Generate original graph iterations times to have an average
//     for (int i = 1; i <= iterations; ++i)
//     {
//         // Get all nodes of graph
//         std::map<uint32_t, std::set<uint32_t>> allGraph;

//         std::chrono::high_resolution_clock::time_point st = std::chrono::high_resolution_clock::now();

//         parallelPragma(x_wt, rrrB1, y3_wt, B4, allGraph);

//         std::chrono::high_resolution_clock::time_point et = std::chrono::high_resolution_clock::now();

//         auto duration = std::chrono::duration_cast<std::chrono::milliseconds> (et-st).count();

//         std::ofstream allNodesFile(path + ".allNodesP", std::ofstream::out | std::ofstream::trunc);


//         uint32_t alledges = 0;
//         // Write nodes to file
//         for (auto mapped : allGraph)
//         {
//             allNodesFile << mapped.first << ": ";
//             for (auto neighbor : mapped.second)
//             {
//                 allNodesFile << neighbor << " ";

//                 alledges += (mapped.first != neighbor) ? 1 : 0;
//             }
//             allNodesFile << std::endl;
//         }

//         allNodesFile.close();


//         t_acum += duration;

//         std::cout << "Loop " << i << " " << solution::time_representation(duration) << " Alledges " << alledges << std::endl;
//     }

//     double average = t_acum/iterations;

//     std::cout << "Promedio " << solution::time_representation(average) << std::endl;

//     return 0;
// }
