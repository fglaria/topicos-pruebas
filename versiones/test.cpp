#include <stdio.h>
#include "vectorclass/vectorclass.h"

// g++ -mavx test.cpp -o test

int main() {
    // define and initialize integer vectors a and b
    Vec4i a(3, 4, 3, 4);
    Vec4i b(6, 6, 7, 7);

    // and the two vectors
    Vec4i d = a & b;


    // Print the results
    for (int i = 0; i < d.size(); i++) {
        printf("%5i ", d[i]);
    }

    printf("\n");

    return 0;
}