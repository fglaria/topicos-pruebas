#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include "vectorclass/vectorclass.h"

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "binary_functions.h"

// g++ -std=c++11 terzo.cpp binary_functions.cpp -o terzo -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64

struct element
{
    unsigned int Partition;
    unsigned int X;
};

int main(int argc, char* argv[]) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA NODO" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);
    // Node to look his neighbors
    const unsigned int searchedNode = atoi(argv[2]);

    std::string sXbin = path+".seqX.bin";
    std::string sYbin = path+".seqBC.bin";
    std::string sB1bin = path+".B-rrr-64.sdsl";
    std::string sB2bin = path+".Bc.bin";

    // File variables for X, B2, Y
    std::ifstream seqXfile (sXbin.c_str(), std::ios::in | std::ios::binary);
    std::ifstream seqBCfile (sYbin.c_str(), std::ios::in | std::ios::binary);
    std::ifstream Bcfile (sB2bin.c_str(), std::ios::in | std::ios::binary | std::ios::ate);


    // SDSL variables to X, B1, Y
    sdsl::wm_int<sdsl::rrr_vector<63>> x_wt;
    sdsl::wm_int<sdsl::rrr_vector<63>> y_wt;
    sdsl::rrr_vector<63> rrrB1;

    // Reading files
    // X & Y
    sdsl::construct(x_wt, sXbin.c_str(), 4);
    sdsl::construct(y_wt, sYbin.c_str(), 4);
    // B1
    load_from_file(rrrB1, sB1bin.c_str());
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);
    // B2
    std::streampos size = Bcfile.tellg();
    unsigned char * B2 = new unsigned char [size];
    Bcfile.seekg (0, std::ios::beg);
    Bcfile.read ((char*)B2, size);
    Bcfile.close();


    // Define and initialize unsigned char vectors for masks
    Vec16uc masksPossibleNeighbors = 0;
    Vec16uc maskSearchedNode = 0;
    Vec16uc maskResult = 0;

    // Vector of neighbors
    std::set<unsigned int> neighbors;


    // Number of partitions (-1 because last 1 in B1 doesn't count)
    uint32_t partitions = rrrB1_rank(rrrB1.size()) - 1;
    // std::cout << "Partitions " << partitions << std::endl;

    // Array for B2 indexes (-1 because first partition starts always at 0)
    uint32_t * B2startIndex = new uint32_t [partitions];
    uint32_t b2Index = 0;

    // First time no previous exists, just 0
    uint32_t currentB1Index = 0;

    // For every partition, get Y and calculate B2 starting index
    for (int pIndex = 0; pIndex < partitions; ++pIndex)
    {
        // std::cout << "Partition " << pIndex + 1 << std::endl;

        // Get Y value
        const uint32_t y = y_wt[pIndex];
        // std::cout << "Y " << y << std::endl;

        // How many bytes make a mask?
        const uint32_t bytesPerMask = 1 + (y-1)/8; // y:[1-8] one byte, (8, inf) 2, 3, 4... bytes

        // Store B2 index
        B2startIndex[pIndex] = b2Index;
        // std::cout << "B2 Index " << b2Index << std::endl;

        // Get next B1 "1" index
        uint32_t nextB1Index = rrrB1_sel(pIndex+2);

        // Is there any searchedNode on this partition?
        uint32_t XisOn = x_wt.rank(nextB1Index - 1, searchedNode);

        // If there is, search for neighbors on that partition
        if(0 != XisOn)
        {
            // Get index of last X
            const uint32_t Xindex = x_wt.select(XisOn, searchedNode);

            // If last searched X index is bigger than current B1 index,
            // it's on this partition. Else is not, so ignore it.
            if(currentB1Index <= Xindex)
            {
                // For every X between currentB1Index and nextB1Index (-1 for last value),
                // get their B2 bitmaps and put them on Vec16uc masksPossibleNeighbors
                for (int iPossibleX = currentB1Index; iPossibleX < nextB1Index; ++iPossibleX)
                {
                    if(Xindex == iPossibleX)
                    {
                        continue;
                    }

                    // If Y is 1, possible X is neighbor, so add it right now!
                    if(1 == y)
                    {
                        neighbors.insert(x_wt[iPossibleX]);
                        continue;
                    }

                    // Get searched X's mask
                    // First, find out position of searched X in partition
                    uint32_t xPosition = Xindex - currentB1Index;
                    // Then, find out index of searched X's mask
                    uint32_t iSearchedMask = b2Index + xPosition*y;
                    // uint8_t

                }
            }
        }

        // Number of elements (X) of this partition
        uint32_t elements = nextB1Index - currentB1Index;
        // std::cout << "Elements " << elements << std::endl;

        // Add Y*elements bits count to B2 Indexes
        b2Index += elements*y;

        // Update latest B1 index
        currentB1Index = nextB1Index;

        // std::cout << std::endl;
    }

    std::cout << std::endl << "Neighbors: ";
    for (std::set<unsigned int>::iterator i = neighbors.begin(); i != neighbors.end(); ++i)
    {
        std::cout << *i << " ";
    }
    std::cout << std::endl;



    return 2;
}
