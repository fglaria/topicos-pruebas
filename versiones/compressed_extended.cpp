// g++ -std=c++11 compressed_extended.cpp binary_functions.cpp -o compressed -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64

#include <iostream>
#include <fstream>
#include <set>
#include "vectorclass/vectorclass.h"

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "binary_functions.h"

// Parallel evaluation of neighbors
void evaluateNeighbors(const Vec16uc* possible, const Vec16uc* searched, const uint32_t* comparedX, std::set<uint32_t>* neighbors)
{
    // Parallel AND
    const Vec16uc masksResult = *possible & *searched;

    // Size of vectors
    const uint8_t maxSize = sizeof(masksResult);

    // std::cout << "masksResult:            ";
    // for (uint8_t i = 0; i < maxSize; ++i)
    // {
    //     std::cout << binary::print(masksResult[i], 8) << " ";
    // }
    // std::cout << std::endl;

    // std::cout << "value X:                ";
    // for (uint8_t i = 0; i < maxSize; i++) {
    //     std::cout << std::setfill(' ') << std::setw(8) << (uint32_t) comparedX[i] << " ";
    // }
    // std::cout << std::endl;

    // Create a boolean vector of comparisons results
    const Vec16cb isNeighbor = masksResult != 0;

    // For each boolean value, write a bit in a short (reversed)
    unsigned short isNeighborShort = to_bits(isNeighbor);
    // std::cout << "isNeighborShort " << binary::print(isNeighborShort, 16) << std::endl;

    // If short is zero, no comparison found any neighbors
    if(0 == isNeighborShort)
    {
        // std::cout << "No neighbors" << std::endl;
    }
    // If is not zero, some neighbor was found
    else
    {
        // For every bit in inNeighbor, let's find wich one
        // It's reversed because isNeighbor is reversed
        for (unsigned char i = 0; i < maxSize; ++i)
        {
            // Get LSB
            const bool bitValue = isNeighborShort & 0x01;

            // Remove LSB
            isNeighborShort >>= 1;

            // Get Possible X
            const unsigned int xN = comparedX[i];

            // If bit is zero, not neighbor
            if(!bitValue)
            {
                // std::cout << "Skipped " << xN << std::endl;
                continue;
            }
            // If bit is one, is neighbor
            else
            {
                // Insert neighbor into set
                neighbors->insert(xN);
                // std::cout << "Neighbor " << xN << std::endl;
            }
        }
    }

    return;
}

int main(int argc, char* argv[]) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA NODO" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);
    // Node to look his neighbors
    const uint32_t searchedNode = atoi(argv[2]);

    std::string sXbin = path+".seqX.bin";
    std::string sB1bin = path+".B-rrr-64.sdsl";
    std::string sB3bin = path+".B3.bin";
    std::string sY2bin = path+".seqY2.bin";

    // SDSL variables to X, B1, B3, Y2
    sdsl::wm_int<sdsl::rrr_vector<63>> x_wt;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wm_int<sdsl::rrr_vector<63>> b3_wt;
    sdsl::wm_int<sdsl::rrr_vector<63>> y2_wt;

    // Reading files
    // X, B3, Y2
    sdsl::construct(x_wt, sXbin.c_str(), 4);
    sdsl::construct(b3_wt, sB3bin.c_str(), 1);
    sdsl::construct(y2_wt, sY2bin.c_str(), 4);
    // B1
    load_from_file(rrrB1, sB1bin.c_str());
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);


    if (argv[3])
    {
        std::cout << "X" << std::endl;
        for (uint32_t i = 0; i < x_wt.size() ; ++i)
        {
            std::cout << i << " " << x_wt[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "Y2" << std::endl;
        for (uint32_t i = 0; i < y2_wt.size() ; ++i)
        {
            std::cout << i << " " << y2_wt[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "B3" << std::endl;
        for (uint32_t i = 0; i < b3_wt.size() ; ++i)
        {
            std::cout << i << " " << b3_wt[i] << std::endl;
        }
        std::cout << std::endl;
    }


    // Define and initialize unsigned char vectors for masks
    Vec16uc masksPossibleNeighbors = 0;
    Vec16uc masksSearchedNode = 0;

    // Max size of vectors
    const uint32_t maxSizeVector = sizeof(masksPossibleNeighbors);

    // Number of masks in Vectors
    uint8_t howManyMasksInVectors = 0;

    // Number of bytes to skip in B3 per partition
    uint32_t currentY = 0;

    // Array of current X compared
    uint32_t comparedX[maxSizeVector];

    // Vector of neighbors
    std::set<uint32_t> neighbors;


    // How many times is there a searched Node in X
    const uint32_t howManyX = x_wt.rank(x_wt.size(), searchedNode);
    // std::cout << "howManyX " << howManyX << std::endl;

    // Let's get every partition with an X, and search for it's neighbors
    for (uint32_t iX = 1; iX <= howManyX; ++iX)
    {
        // Index of iX's ocurrence in X
        const uint32_t xIndex = x_wt.select(iX, searchedNode);
        // std::cout << "xIndex " << xIndex << std::endl;
        // Number of 1s in B1 until xIndex, indicates current partition
        const uint32_t partition = rrrB1_rank(xIndex + 1);
        // std::cout << std::endl << "Partition " << partition << "; iX " << iX <<
        //     "; xIndex " << xIndex << std::endl;

        // Index of 1 in B1 of this partition
        const uint32_t currentB1Index = rrrB1_sel(partition);
        // Index of 1 in B1 of next partition
        const uint32_t nextB1Index = rrrB1_sel(partition + 1);
        // Number of elements in partition
        const uint32_t elements = nextB1Index - currentB1Index;
        // std::cout << "currentB1Index " << currentB1Index << "; nextB1Index " << nextB1Index <<
        //     "; elements " << elements << std::endl;

        // Index of X inside partition (reference to currentB1Index)
        const uint32_t xPartitionIndex = xIndex - currentB1Index;
        // std::cout << "xPartitionIndex " << xPartitionIndex << std::endl;

        // Number of bytes to skip in B3
        const uint32_t nextY = y2_wt[partition];
        // Number of bytes to read
        const uint32_t bytesInB3 = nextY - currentY;
        // Number of bytes per mask
        const uint32_t bytesPerMask = bytesInB3/elements;
        // std::cout << "currentY " << currentY << "; nextY " << nextY <<
        //     "; bytesInB3 " << bytesInB3 << "; bytesPerMask " << bytesPerMask << std::endl;


        // For every element in partition, compare to searchedNode and check if neighbors
        for (uint32_t iPartition = 0; iPartition < elements; ++iPartition)
        {
            // std::cout << "iPartition " << iPartition << std::endl;

            // Get current X to compare
            const uint32_t currentX = x_wt[currentB1Index + iPartition];
            // std::cout << "currentX " << currentX << std::endl;

            // If it's the same searchedNode, skip it
            if(currentX == searchedNode)
            {
                // std::cout << "Skip" << std::endl;
                continue;
            }


            // Count bytes copied to vector
            uint32_t bytesToCopy = 0;

            // While the are bytes to copy of same X
            while (bytesToCopy != bytesPerMask)
            {
                // std::cout << "bytesToCopy " << bytesToCopy << std::endl;

                // Byte of Mask of searchedNode
                const unsigned char maskOfSearchedNode = b3_wt[currentY + xPartitionIndex + bytesToCopy];
                // std::cout << "maskOfSearchedNode " << binary::print(maskOfSearchedNode, 8) << std::endl;

                // If mask is only one byte, and value 1, IT'S NEIGHBOR
                if ((1 == bytesPerMask) && (1 == maskOfSearchedNode))
                {
                    neighbors.insert(currentX);
                    break;
                }

                // Byte of mask of Possible Neighbor
                const unsigned char maskOfPossibleNeighbor = b3_wt[currentY + iPartition*bytesPerMask + bytesToCopy];
                // std::cout << "maskOfPossible     " << binary::print(maskOfPossibleNeighbor, 8) << std::endl;

                // Put masks on vectors
                masksPossibleNeighbors.insert(howManyMasksInVectors, maskOfPossibleNeighbor);
                masksSearchedNode.insert(howManyMasksInVectors, maskOfSearchedNode);

                // Store X value
                comparedX[howManyMasksInVectors] = currentX;

                // Plus one to how many bytes are in vectors
                howManyMasksInVectors += 1;

                // If vectors are full
                if (maxSizeVector == howManyMasksInVectors)
                {
                    evaluateNeighbors(&masksPossibleNeighbors, &masksSearchedNode, comparedX, &neighbors);

                    // // std::cout << "masksPossibleNeighbors: ";
                    // // for (int i = 0; i < 16; ++i)
                    // // {
                    // //     std::cout << binary::print(masksPossibleNeighbors[i], 8) << " ";
                    // // }
                    // // std::cout << std::endl;

                    // // std::cout << "masksSearchedNode:      ";
                    // // for (int i = 0; i < 16; ++i)
                    // // {
                    // //     std::cout << binary::print(masksSearchedNode[i], 8) << " ";
                    // // }
                    // // std::cout << std::endl;

                    // Clean vectors
                    masksPossibleNeighbors = 0;
                    masksSearchedNode = 0;

                    howManyMasksInVectors = 0;
                }

                // Next byte to copy mask
                bytesToCopy += 1;
            }
        }

        // Update currentY
        currentY = nextY;
    }

    // // std::cout << "masksPossibleNeighbors: ";
    // // for (int i = 0; i < 16; ++i)
    // // {
    // //     std::cout << binary::print(masksPossibleNeighbors[i], 8) << " ";
    // // }
    // // std::cout << std::endl;

    // // std::cout << "masksSearchedNode:      ";
    // // for (int i = 0; i < 16; ++i)
    // // {
    // //     std::cout << binary::print(masksSearchedNode[i], 8) << " ";
    // // }
    // // std::cout << std::endl;


    evaluateNeighbors(&masksPossibleNeighbors, &masksSearchedNode, comparedX, &neighbors);

    // Print neighbors
    std::cout << searchedNode << ": ";
    for (std::set<unsigned int>::iterator i = neighbors.begin(); i != neighbors.end(); ++i)
    {
        std::cout << *i << " ";
    }
    std::cout << std::endl;


    return 0;
}
