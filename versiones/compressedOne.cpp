// g++ -std=c++11 compressedOne.cpp solution.cpp binary_functions.cpp -o compressedOne -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64

#include <set>
#include <fstream>
#include <sstream>
#include <string>
#include "solution.h"

int main(int argc, char* argv[]) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA NODO" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Set of nodes to look their neighbors
    std::set<uint32_t> searchedNodes;
    // To store node values
    uint32_t number = atoi(argv[2]);

    searchedNodes.insert(number);

    solution::parallel(path, searchedNodes);

    // // Print neighbors
    // for (std::map<uint32_t, std::set<uint32_t>>::iterator iMap = neighbors.begin(); iMap != neighbors.end(); ++iMap)
    // {
    //     std::cout << iMap->first << ": ";

    //     for (std::set<uint32_t>::iterator iSet = iMap->second.begin(); iSet != iMap->second.end(); ++iSet)
    //     {
    //         std::cout << *iSet << " ";
    //     }

    //     std::cout << std::endl;
    // }

    // std::cout << std::endl;

    return 0;
}
