#ifndef TOPICOS_BLOCK
#define TOPICOS_BLOCK

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "binary_functions.h"

namespace topicos
{
    struct block
    {
        std::vector<unsigned int> X;
        std::vector<unsigned char> B;
        unsigned int Y;
    };

    struct element
    {
        unsigned int Block;
        unsigned int X;
    };

    struct block * getBlocks(char * pathC, unsigned int* amount);
    struct block * getPartitions(char * pathC, unsigned int* amount);
}

#endif
