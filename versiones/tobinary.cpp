#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

int main(int argc, char* argv[]) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " ARCHIVO SALIDA" << std::endl;
        return -1;
    }

    std::ifstream fileInput(argv[1]);
    std::string contentS;

    if (getline(fileInput, contentS))
    {
        fileInput.close();
    }
    else
    {
        std::cout << "Unable to open file fileInput" << std::endl;
        return -1;
    }


    std::ofstream output(argv[2], std::ios::out | std::ios::binary);

    std::stringstream content(contentS);

    unsigned int bit;
    while (content >> bit)
    {
        char b[1];
        if(!bit){
            b[0] = false;
        }
        else
        {
            b[0] = true;
        }

        output.write(b, 1);
    }

    output.close();




    fileInput.open(argv[2], std::ios::in | std::ios::binary | std::ios::ate);

    std::streampos size = fileInput.tellg();
    char * memblock = new char [size];
    fileInput.seekg (0, std::ios::beg);
    fileInput.read (memblock, size);
    fileInput.close();

    for (int i = 0; i < size; ++i)
    {
        std::cout << (int) memblock[i] << " ";
    }
    std::cout << std::endl;

    return 1;
}
