#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <string>

int main(int argc, char const *argv[])
{
    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Path to X seq file
    std::string sX = path+".seqX";

    // stream to X file
    std::ifstream seqXfile (sX.c_str());

    // Reading X files
    if (getline(seqXfile, sX))
    {
        seqXfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqXfile" << std::endl;
        return -1;
    }


    // String to Vector of int
    std::stringstream ssX(sX);

    std::set<uint32_t> X;

    uint32_t number;

    while (ssX >> number)
    {
        X.insert(number);
    }


    std::ofstream Xfile(path + ".setX", std::ofstream::out | std::ofstream::trunc);

    for (std::set<uint32_t>::iterator x = X.begin(); x != X.end(); ++x)
    {
        Xfile << *x << " ";
    }

    Xfile.close();

    return 0;
}