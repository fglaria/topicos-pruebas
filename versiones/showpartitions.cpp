// g++ showpartitions.cpp block.cpp binary_functions.cpp -o showpartitions

#include <vector>
#include <stdlib.h>
#include "block.h"
#include "binary_functions.h"

using namespace std;

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    unsigned int totalBlocks;
    topicos::block * allBlocks = topicos::getPartitions(argv[1], &totalBlocks);

    cout << "Total Blocks: " << totalBlocks << endl;


    if(3 == argc)
    {
        unsigned int block = atoi(argv[2]);

        unsigned int y = allBlocks[block].Y;

        cout << "Partition " << block + 1 << endl;

        cout << "X ";
        for (std::vector<unsigned int>::iterator i = allBlocks[block].X.begin(); i != allBlocks[block].X.end(); ++i)
        {
            cout << *i << " ";
        }
        cout << endl;

        cout << "B3 ";
        for (std::vector<unsigned char>::iterator i = allBlocks[block].B.begin(); i != allBlocks[block].B.end(); ++i)
        {
            cout << binary::print(*i, 8) << " ";
        }
        cout << endl;

        cout << "Y2 " << allBlocks[block].Y << endl << endl;
    }
    else
    {
        // Print all blocks
        for (unsigned int aBi=0; aBi < totalBlocks; ++aBi)
        {
            unsigned int y = allBlocks[aBi].Y;

            cout << "Partition " << aBi + 1 << endl;

            cout << "X ";
            for (std::vector<unsigned int>::iterator i = allBlocks[aBi].X.begin(); i != allBlocks[aBi].X.end(); ++i)
            {
                cout << *i << " ";
            }
            cout << endl;

            cout << "B3 ";
            for (std::vector<unsigned char>::iterator i = allBlocks[aBi].B.begin(); i != allBlocks[aBi].B.end(); ++i)
            {
                cout << binary::print(*i, 8) << " ";
            }
            cout << endl;

            cout << "Y2 " << allBlocks[aBi].Y << endl;

            cout << endl;
        }
    }

    return 0;
}
