// g++ secondo.cpp block.cpp binary_functions.cpp -o secondo

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <iomanip>
#include "vectorclass/vectorclass.h"
#include "block.h"
#include "binary_functions.h"

using namespace std;

string printVector16c(Vec16uc v)
{
    std::stringstream out;

    for (int i = 0; i < v.size(); ++i)
    {
        out << binary::print(v[i], 8)  << " ";
    }

    return out.str();
}

int main(int argc, char* argv[]) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA NODO" << std::endl;
        return -1;
    }

    unsigned int numberNode = atoi(argv[2]);

    unsigned int totalBlocks;
    topicos::block * allBlocks = topicos::getBlocks(argv[1], &totalBlocks);


    // Show block if verbose
    if(argv[3])
    {
        // Print blocks
        for (unsigned int aBi=0; aBi<totalBlocks; ++aBi)
        {
            unsigned int y = allBlocks[aBi].Y;

            cout << "Bloque " << aBi + 1 << endl;

            cout << "X ";
            for (std::vector<unsigned int>::iterator i = allBlocks[aBi].X.begin(); i != allBlocks[aBi].X.end(); ++i)
            {
                cout << *i << " ";
            }
            cout << endl;

            cout << "B ";
            for (std::vector<unsigned char>::iterator i = allBlocks[aBi].B.begin(); i != allBlocks[aBi].B.end(); ++i)
            {
                cout << binary::print(*i, 8) << " ";
            }
            cout << endl;

            cout << "Y " << allBlocks[aBi].Y << endl << endl;
        }
    }



    // Define and initialize unsigned char vectors for masks
    Vec16uc masksToSearch = 0;
    Vec16uc maskFounded = 0;
    Vec16uc maskResult = 0;

    // Array to store searched x indexes to find neighbors
    topicos::element xIndexes[16];

    // Vector to store X and Y values
    vector<unsigned int> xList;
    vector<unsigned int> yList;

    // Vector of neighbors
    set<unsigned int> neighbors;

    // Number of elements in B
    unsigned short countInB = 0;

    unsigned int vectorUseTurn = 0;

    // Search for number on each block
    for (unsigned int indexBlock=0; indexBlock<totalBlocks; ++indexBlock)
    {
        // Search for number on X's block
        for (int iX = 0; iX < allBlocks[indexBlock].X.size(); ++iX)
        {
            // If number found
            if(numberNode == allBlocks[indexBlock].X[iX])
            {
                cout << "Encontrado en Bloque " << indexBlock + 1 << endl;

                unsigned int y = allBlocks[indexBlock].Y;

                // How many bytes compose a B mask
                const unsigned int bytesPerMask = 1 + (y-1)/8; // y:[1-8] one byte, (8, inf) 2, 3, 4... bytes

                bool checkInX = false;

                // Count bytes to check
                unsigned int bytesToCheck = 0;

                // How many bits are valid to read from mask
                unsigned int bitsToRead = y - 8*(bytesPerMask - 1);

                // While there are bytes to read...
                while (bytesToCheck != bytesPerMask)
                {
                    // Index of masks to check
                    const unsigned int iBXcopy = iX*bytesPerMask + bytesToCheck;

                    // Get mask value
                    unsigned char mask = allBlocks[indexBlock].B[iBXcopy];

                    // For each valid bit in mask, check it value
                    for (int slip = 0; slip < bitsToRead; ++slip)
                    {
                        // Take only lsb from value, and check if 0 or 1
                        unsigned char bitValue = (mask >> slip) & 0x01;

                        // If 0, don't check any more, search neighbors in X
                        if(!bitValue)
                        {
                            checkInX = true;

                            break;
                        }
                    }

                    // If mask had 0, stop checking
                    if(checkInX)
                    {
                        break;
                    }

                    bytesToCheck += 1;
                    bitsToRead = 8;
                }

                // If mask didn't had any 0, no need to look any other masks, all X are NEIGHBORS
                if(!checkInX)
                {
                    cout << "y mask MAX en Bloque :" << indexBlock + 1 << endl;
                    neighbors.insert(allBlocks[indexBlock].X.begin(), allBlocks[indexBlock].X.end());

                    continue;
                }


                // Search for neighbors
                // For every X, compare B masks with searched one
                for (int iXCompare = 0; iXCompare < allBlocks[indexBlock].X.size(); ++iXCompare)
                {
                    // Don't compare the same X
                    if(iX != iXCompare)
                    {
                        // Count bytes copied to vector
                        unsigned int bytesToCopy = 0;

                        // While there are bytes to copy...
                        while (bytesToCopy != bytesPerMask)
                        {
                            // Indexes for masks to copy
                            const unsigned int iBcopy = iXCompare*bytesPerMask + bytesToCopy;
                            const unsigned int iBXcopy = iX*bytesPerMask + bytesToCopy;

                            // Copy masks to vectors
                            masksToSearch.insert(countInB, allBlocks[indexBlock].B[iBcopy]);
                            maskFounded.insert(countInB, allBlocks[indexBlock].B[iBXcopy]);

                            // Store compared X index in case is neighbor
                            xIndexes[countInB].Block = indexBlock;
                            xIndexes[countInB].X = iXCompare;

                            // Push X and Y values
                            xList.push_back(allBlocks[indexBlock].X[iXCompare]);
                            yList.push_back(y);

                            bytesToCopy += 1;

                            countInB += 1;

                            // Vector is full, show result & empty variables
                            if (16 == countInB)
                            {
                                // Show how many times vectors has been used
                                vectorUseTurn += 1;
                                cout << "Full vector N times: " << vectorUseTurn << endl;

                                // Restart count
                                countInB = 0;

                                // Compare masks
                                maskResult = masksToSearch & maskFounded;

                                // Create a boolean vector of comparisons results
                                Vec16cb isNeighbor = maskResult != 0;

                                // For each boolean value, write a bit in a short
                                unsigned short isNeighborShort = to_bits(isNeighbor);

                                // If short is zero, no comparison found any neighbors
                                if(0 == isNeighborShort)
                                {
                                    cout << "No neighbors" << endl;
                                }
                                else // The is at least one neighbor
                                {
                                    cout << endl;
                                    cout << "SearchB: " << printVector16c(masksToSearch) << endl;

                                    cout << "maskX  : " << printVector16c(maskFounded) << endl;

                                    cout << "value X: ";
                                    for (int i = 0; i < xList.size(); i++) {
                                        cout << setfill(' ') << setw(8) << (int) xList[i] << " ";
                                    }
                                    cout << endl;

                                    cout << "value Y: ";
                                    for (int i = 0; i < yList.size(); i++) {
                                        cout << setfill(' ') << setw(8) << (int) yList[i] << " ";
                                    }
                                    cout << endl;

                                    cout << "result : " << printVector16c(maskResult) << endl;

                                    cout << endl;

                                    for (unsigned char i = 15; i > 0; --i)
                                    {
                                        bool bitValue = isNeighborShort & 0x01;

                                        isNeighborShort >>= 1;

                                        if(!bitValue)
                                        {
                                            unsigned char iN = 15 - i;
                                            const unsigned int iBlockN = xIndexes[iN].Block;
                                            const unsigned int xN = allBlocks[iBlockN].X[xIndexes[iN].X];

                                            cout << "Skipped " << xN << endl;
                                            continue;
                                        }
                                        else
                                        {
                                            unsigned char iN = 15 - i;
                                            const unsigned int iBlockN = xIndexes[iN].Block;
                                            const unsigned int xN = allBlocks[iBlockN].X[xIndexes[iN].X];

                                            neighbors.insert(xN);
                                            cout << "Neighbor " << xN << endl;
                                        }
                                    }
                                }

                                masksToSearch = 0;
                                maskFounded = 0;
                                xList.clear();
                                yList.clear();
                            }
                        }

                    }
                }

            }
        }
    }

    // Show results for last values
    maskResult = masksToSearch & maskFounded;

    // Create a boolean vector of comparisons results
    Vec16cb isNeighbor = maskResult != 0;

    // For each boolean value, write a bit in a short
    unsigned short isNeighborShort = to_bits(isNeighbor);

    // If short is zero, no comparison found any neighbors
    if(0 == isNeighborShort)
    {
        // cout << "No neighbors" << endl;
    }
    else // The is at least one neighbor
    {

        cout << endl;
        cout << "SearchB: " << printVector16c(masksToSearch) << endl;

        cout << "masks  : " << printVector16c(maskFounded) << endl;

        cout << "In X   : ";
        for (int i = 0; i < xList.size(); i++) {
            cout << setfill(' ') << setw(8) << (int) xList[i] << " ";
        }
        cout << endl;

        cout << "In Y   : ";
        for (int i = 0; i < yList.size(); i++) {
            cout << setfill(' ') << setw(8) << (int) yList[i] << " ";
        }
        cout << endl;

        cout << "result : " << printVector16c(maskResult) << endl;

        for (unsigned char i = 0; i < countInB; ++i)
        {
            bool bitValue = isNeighborShort & 0x01;

            isNeighborShort >>= 1;

            if(!bitValue)
            {
                const unsigned int iBlockN = xIndexes[i].Block;
                const unsigned int xN = allBlocks[iBlockN].X[xIndexes[i].X];

                cout << "Skipped2 " << xN << endl;
                continue;
            }
            else
            {
                const unsigned int iBlockN = xIndexes[i].Block;
                const unsigned int xN = allBlocks[iBlockN].X[xIndexes[i].X];

                neighbors.insert(xN);
                cout << "Neighbor3 " << xN << endl;
            }
        }

        // // References to check if some byte of X already checked & neighbor
        // unsigned int xOldN = numberNode; // Searched value (shouldn't be on it)
        // bool bOldN = false;

        // // Search neighbors in boolean vector of comparisons
        // for (int iN = 0; iN < countInB; ++iN)
        // {
        //     const unsigned int iBlockN = xIndexes[iN].Block;
        //     const unsigned int xN = allBlocks[iBlockN].X[xIndexes[iN].X];

        //     xIndexes[iN].X;

        //     if(bOldN && (xOldN == xN))
        //     {
        //         cout << "Skipped " << xN << endl;
        //         continue;
        //     }

        //     if(isNeighbor[iN])
        //     {
        //         cout << "Neighbor " << xN << endl;
        //         neighbors.insert(xN);
        //         bOldN = true;
        //     }
        //     else
        //     {
        //         cout << "NO Neighbor " << xN << endl;
        //         bOldN = false;
        //     }

        //     xOldN = xN;
        // }

        cout << endl;
    }



    cout << endl << "Neighbors: ";
    for (std::set<unsigned int>::iterator i = neighbors.begin(); i != neighbors.end(); ++i)
    {
        cout << *i << " ";
    }
    cout << endl;


    return 0;
}