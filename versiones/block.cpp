#include "block.h"

namespace topicos
{
    struct block * getBlocks(char * pathC, unsigned int* amount)
    {
        std::string path = pathC;

        std::string sX = path+".seqX";
        std::string sY = path+".seqBC";
        std::string sB1 = path+".B";
        std::string sB2 = path+".Bc";

        std::ifstream seqXfile (sX.c_str());
        std::ifstream seqBCfile (sY.c_str());
        std::ifstream Bfile (sB1.c_str());
        std::ifstream Bcfile (sB2.c_str());

        // Reading files
        // X
        if (getline(seqXfile, sX))
        {
            seqXfile.close();
        }
        else
        {
            std::cout << "Unable to open file seqXfile" << std::endl;
            return NULL;
        }

        // Y
        if (getline (seqBCfile, sY))
        {
            seqBCfile.close();
        }
        else
        {
            std::cout << "Unable to open file seqBCfile" << std::endl;
            return NULL;
        }

        // B1
        if (getline (Bfile, sB1))
        {
            Bfile.close();
        }
        else
        {
            std::cout << "Unable to open file Bfile" << std::endl;
            return NULL;
        }

        // B2
        if (getline (Bcfile, sB2))
        {
            Bcfile.close();
        }
        else
        {
            std::cout << "Unable to open file Bcfile" << std::endl;
            return NULL;
        }




        // String to Vector of int
        std::stringstream ssX(sX), ssY(sY), ssB1(sB1), ssB2(sB2);
        std::vector<unsigned int> X, Y, B1, B2;

        unsigned int number;

        while (ssX >> number)
        {
            X.push_back(number);
        }

        while (ssY >> number)
        {
            Y.push_back(number);
        }

        while (ssB1 >> number)
        {
            B1.push_back(number);
        }

        // std::string sNumber;
        while (ssB2 >> number)
        {
            B2.push_back(number);
        }


        // Create all needed blocks
        block* allBlocks;
        allBlocks = new block[Y.size()];

        *amount = Y.size();

        int block_index = -1;
        unsigned int y = 0;

        unsigned int bIndex = 0;

        for (std::vector<unsigned int>::iterator iB1 = B1.begin(); iB1 != B1.end()-1; ++iB1)
        {
            // New block
            if(1 == *iB1)
            {
                block_index += 1;
                y = Y[block_index];
                allBlocks[block_index].Y = y;
            };

            unsigned int index = iB1 - B1.begin();

            unsigned int x = X[index];

            (allBlocks[block_index].X).push_back(x);

            unsigned int bitsToProcess = y;
            unsigned char value = 0;

            for (unsigned int i = bIndex; i != bIndex + y; ++i)
            {
                value <<= 1;
                value += B2[i];

                bitsToProcess -= 1;

                if (0 == bitsToProcess%8)
                {
                    allBlocks[block_index].B.push_back( value );
                    value = 0;
                }
            }

            bIndex += y;

        }

        return allBlocks;
    }


    struct block * getPartitions(char * pathC, unsigned int* amount)
    {
        std::string path = pathC;

        std::string sX = path+".seqX";
        std::string sY2 = path+".seqY2";
        std::string sB1 = path+".B";
        std::string sB3 = path+".B3";

        std::ifstream seqXfile (sX.c_str());
        std::ifstream seqY2file (sY2.c_str());
        std::ifstream Bfile (sB1.c_str());
        std::ifstream B3file (sB3.c_str());

        // Reading files
        // X
        if (getline(seqXfile, sX))
        {
            seqXfile.close();
        }
        else
        {
            std::cout << "Unable to open file seqXfile" << std::endl;
            return NULL;
        }

        // Y
        if (getline (seqY2file, sY2))
        {
            seqY2file.close();
        }
        else
        {
            std::cout << "Unable to open file seqY2file" << std::endl;
            return NULL;
        }

        // B1
        if (getline (Bfile, sB1))
        {
            Bfile.close();
        }
        else
        {
            std::cout << "Unable to open file Bfile" << std::endl;
            return NULL;
        }

        // B2
        if (getline (B3file, sB3))
        {
            B3file.close();
        }
        else
        {
            std::cout << "Unable to open file B3file" << std::endl;
            return NULL;
        }




        // String to Vector of int
        std::stringstream ssX(sX), ssY2(sY2), ssB1(sB1), ssB3(sB3);
        std::vector<unsigned int> X, Y2, B1, B3;

        unsigned int number;

        while (ssX >> number)
        {
            X.push_back(number);
        }

        while (ssY2 >> number)
        {
            Y2.push_back(number);
        }

        while (ssB1 >> number)
        {
            B1.push_back(number);
        }

        // std::string sNumber;
        while (ssB3 >> number)
        {
            B3.push_back(number);
        }


        // Create all needed blocks
        block* allBlocks;
        allBlocks = new block[Y2.size()];

        *amount = Y2.size();

        int block_index = -1;
        unsigned int currentY = 0;
        unsigned int nextY = 0;

        unsigned int bIndex = 0;

        unsigned int bytesPerX = 0;

        for (std::vector<unsigned int>::iterator iB1 = B1.begin(); iB1 != B1.end()-1; ++iB1)
        {
            // New block
            if(1 == *iB1)
            {
                block_index += 1;

                nextY = Y2[block_index + 1];
                allBlocks[block_index].Y = nextY;

                for (int i = currentY; i < nextY; ++i)
                {
                    allBlocks[block_index].B.push_back(B3[i]);
                }

                currentY = nextY;
            };

            unsigned int index = iB1 - B1.begin();

            unsigned int x = X[index];

            (allBlocks[block_index].X).push_back(x);
        }

        return allBlocks;
    }
}
