// g++ -std=c++11 compressed.cpp solution.cpp binary_functions.cpp -o compressed -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64

#include <set>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/times.h>
#include "solution.h"

/* Time meassuring */
double ticks;
struct tms t1,t2;

void start_clock() {
    times (&t1);
}

double stop_clock() {
    times (&t2);
    return (t2.tms_utime-t1.tms_utime)/ticks;
}
/* end Time meassuring */

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Path to X seq file
    std::string sX = path+".seqX";
    // stream to X file
    std::ifstream seqXfile (sX.c_str());

    // Reading X files
    if (getline(seqXfile, sX))
    {
        seqXfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqXfile" << std::endl;
        return -1;
    }

    // String to Vector of int
    std::stringstream ssX(sX);
    // Set of nodes to look their neighbors
    std::set<uint32_t> searchedNodes;
    // To store node values
    uint32_t number;

    // For every node, extract value and add it to set
    while (ssX >> number)
    {
        searchedNodes.insert(number);
    }



    double t_acum = 0.0;
    uint32_t iterations = 20;

    for (int i = 1; i <= iterations; ++i)
    {
        ticks = (double)sysconf(_SC_CLK_TCK);
        start_clock();

        std::map<uint32_t, std::set<uint32_t>> allGraph = solution::parallel(path, searchedNodes);

        std::ofstream allNodesFile(path + ".allNodes", std::ofstream::out | std::ofstream::trunc);

        for (auto mapped : allGraph)
        {
            allNodesFile << mapped.first << ": ";
            for (auto neighbor : mapped.second)
            {
                allNodesFile << neighbor << " ";
            }
            allNodesFile << std::endl;
        }

        allNodesFile.close();

        double t = stop_clock()*1000;
        t_acum += t;

        std::cout << "Loop " << i << " " << t << " [ms]" << std::endl;
    }

    std::cout << "Promedio: " << t_acum/iterations << " [ms]" << std::endl;

    // // Print neighbors
    // for (std::map<uint32_t, std::set<uint32_t>>::iterator iMap = neighbors.begin(); iMap != neighbors.end(); ++iMap)
    // {
    //     std::cout << iMap->first << ": ";

    //     for (std::set<uint32_t>::iterator iSet = iMap->second.begin(); iSet != iMap->second.end(); ++iSet)
    //     {
    //         std::cout << *iSet << " ";
    //     }

    //     std::cout << std::endl;
    // }

    // std::cout << std::endl;

    return 0;
}
