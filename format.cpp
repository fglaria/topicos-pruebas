// g++ -std=c++11 format.cpp binary_functions.cpp -o format

#include <stdio.h>
#include <stdlib.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>
#include "binary_functions.h"

template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seq, std::ofstream &seqbin){
    type *buffer = new type[numbers.size()];
    uint32_t end = numbers.end() - numbers.begin();

    for(uint32_t i=0; i<end; ++i)
    {
        seq << (uint32_t) numbers[i] << " ";
        buffer[i] = numbers[i];
    }

    seqbin.write((char *)&*buffer, (numbers.size())*sizeof(type));
    delete[] buffer;
}

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    std::string sX = path + ".seqX";
    std::string sY = path + ".seqBC";
    std::string sB1 = path + ".B";
    std::string sB2 = path + ".Bc";

    std::ifstream seqXfile (sX.c_str());
    std::ifstream seqBCfile (sY.c_str());
    std::ifstream Bfile (sB1.c_str());
    std::ifstream Bcfile (sB2.c_str());

    if (getline (seqXfile, sX))
    {
        seqXfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqBCfile" << std::endl;
        return -1;
    }

    // Reading files
    // Y
    if (getline (seqBCfile, sY))
    {
        seqBCfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqBCfile" << std::endl;
        return -1;
    }

    // B1
    if (getline (Bfile, sB1))
    {
        Bfile.close();
    }
    else
    {
        std::cout << "Unable to open file Bfile" << std::endl;
        return -1;
    }

    // B2
    if (getline (Bcfile, sB2))
    {
        Bcfile.close();
    }
    else
    {
        std::cout << "Unable to open file Bcfile" << std::endl;
        return -1;
    }




    // String to Vector of uint32_t
    std::stringstream ssY(sY), ssB1(sB1), ssB2(sB2), ssX(sX);
    std::vector<uint32_t> Y, B1, B2, X;

    uint32_t number;

    while (ssY >> number)
    {
        Y.push_back(number);
    }

    while (ssX >> number)
    {
        X.push_back(number);
    }

    while (ssB1 >> number)
    {
        B1.push_back(number);
    }
    // Removes last 1
    B1.pop_back();

    while (ssB2 >> number)
    {
        B2.push_back(number);
    }
    // std::cout << "B2.size() " << B2.size() << std::endl;


    // // Prueba de espacio original
    // std::ofstream B2file(path + ".B2", std::ofstream::out | std::ofstream::trunc);
    // std::ofstream B2fileBin(path + ".B2.bin", std::ofstream::binary | std::ofstream::trunc);

    // // Write B4 files, string and binary
    // writeFileandBinary(B2, B2file, B2fileBin);

    // return -1;
    // // Fin prueba de espacio


    // B2 in bytes (NEW B2) B4?
    std::vector<unsigned char> B4;
    // Y3 count number of bytes per partition
    std::vector<uint32_t> Y3;

    // Index to count on wich partition per loop
    int64_t partition_index = -1;
    // Count how many bytes are there in B4
    uint32_t bytesInB4 = 0;
    // Index in B2
    uint32_t b2Index = 0;
    // Current y per loop
    uint32_t y = 0;



    for (auto iB1 : B1)
    {

        // std::cout << "partition_index " << partition_index << std::endl;
        // New partition
        if (1 == iB1)
        {
            partition_index += 1;
            y = Y[partition_index];

            Y3.push_back(bytesInB4);

            if (1 == y)
            {
                bytesInB4 += 1;
                continue;
            }
        }

        if (1 == y)
        {
            continue;
        }



        uint32_t bitsToProcess = y;
        unsigned char value = 0;

        // std::cout << std::endl;

        // std::cout << "Y.size() " << Y.size() << std::endl;

        // std::cout << "b2Index " << b2Index << std::endl;
        // std::cout << "y " << y << std::endl;
        // std::cout << "b2Index + y " << b2Index + y << std::endl;

        for (unsigned int i = b2Index; i < b2Index + y; ++i)
        {
            value <<= 1;
            value += B2[i];

            bitsToProcess -= 1;

            if (0 == bitsToProcess%8)
            {
                B4.push_back(value);
                bytesInB4 += 1;

                value = 0;
            }
        }

        // std::cout << std::endl;

        b2Index += y;
    }

    Y3.push_back(bytesInB4);



    // Output files
    std::ofstream Y3file(path + ".seqY3", std::ofstream::out | std::ofstream::trunc);
    std::ofstream Y3fileBin(path + ".seqY3.bin", std::ofstream::binary | std::ofstream::trunc);
    std::ofstream B4file(path + ".B4", std::ofstream::out | std::ofstream::trunc);
    std::ofstream B4fileBin(path + ".B4.bin", std::ofstream::binary | std::ofstream::trunc);
    std::ofstream XfileBin(path + ".seqX.bin", std::ofstream::binary | std::ofstream::trunc);
    std::ofstream Xfile(path + ".seqXX", std::ofstream::out | std::ofstream::trunc);

    writeFileandBinary(X, Xfile, XfileBin);
    // Write Y3 files, string and binary
    writeFileandBinary(Y3, Y3file, Y3fileBin);

    // Write B4 files, string and binary
    writeFileandBinary(B4, B4file, B4fileBin);





    if (argv[2])
    {
        std::cout << "B1 ";
        for (std::vector<unsigned int>::iterator i = B1.begin(); i != B1.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl;

        std::cout << "B2 ";
        for (std::vector<unsigned int>::iterator i = B2.begin(); i != B2.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl;

        std::cout << "Y ";
        for (std::vector<unsigned int>::iterator i = Y.begin(); i != Y.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl << std::endl;

        std::cout << "B4" << std::endl;
        for (std::vector<unsigned char>::iterator i = B4.begin(); i != B4.end(); ++i)
        {
            std::cout << binary::print(*i, 8) << " ";
        }
        std::cout << std::endl;

        std::cout << "Y3" << std::endl;
        for (std::vector<uint32_t>::iterator i = Y3.begin(); i != Y3.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl;
    }



    return 0;
}
