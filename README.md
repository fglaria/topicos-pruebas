# INSTRUCCIONES

## 1. Compilar format.cpp
`g++ -std=c++11 format.cpp binary_functions.cpp -o format`

## 2. Ejecutar format sobre archivos entregados por formace
`./format2 ruta/base_nombre_archivos`

## 3. Compilar compress.cpp
`g++ -std=c++11 compress.cpp -o compress -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

## 4. Ejecutar compress sobre archivos creados por format
`./compress ruta/base_nombre_archivos`

## 5. El listado de métodos implementados es el siguiente:
* **onlySDSL.cpp**: Solo ejecuta el acceso a las estructuras compactas (sin B4 comprimido). No reconstruye grafo.

  `onlySDSL.cpp solution.cpp -o onlySDSL -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **secuencial.cpp**: Recontruye grafo con acceso de manera secuencial a X. No usa B4 comprimido.

  `g++ -std=c++11 secuencial.cpp solution.cpp -o secuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **compSecuencial.cpp**: Método secuencial, pero usando B4 comprimido.

  `g++ -std=c++11 compSecuencial.cpp solution.cpp -o compSecuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **parallel.cpp**: Método secuencial, pero usando librería SIMD. Sin B4 comprimido.

  `g++ -std=c++11 parallel.cpp solution.cpp -o parallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **pragmaMethod.cpp**: Método secuencial, pero aplicando paralelismo pragma cuando se pueda. Sin B4 comprimido.

  `g++ -std=c++11 pragmaMethod.cpp solution.cpp -o pragmaMethod -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **jumpingMethod.cpp**: Método de acceso aleatorio, según dónde se encuentre el X buscado. Sin B4 comprimido.

  `g++ -std=c++11 jumpingMethod.cpp solution.cpp -o jumpingMethod -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **jumpingParallel.cpp**: Método de acceso aleatorio, aplicando paralelismo pragma. Sin B4 comprimido.

  `g++ -std=c++11 jumpingParallel.cpp solution.cpp -o jumpingParallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **mapUnordered.cpp**: Método secuencial, con mapa no ordenado. Sin B4 comprimido.

  `g++ -std=c++11 mapUnordered.cpp solution.cpp -o mapUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **allUnordered.cpp**: Método secuencial, con mapa y set no ordenados. Sin B4 comprimido.

  `g++ -std=c++11 allUnordered.cpp solution.cpp -o allUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* **unorderedPragma.cpp**: Método secuencial, con mapa y set no ordenado, y paralelismo pragma. Sin B4 comprimido.

  `g++ -std=c++11 unorderedPragma.cpp solution.cpp -o unorderedPragma -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG`

* *Para compilar y ejecutar todos los métodos, usar runAll.sh*

  `bash runAll.sh ruta/base_nombre_archivos ITERACIONES (compile)`

  *Cambiar ITERACIONES por la cantidad de veces a ejecutar los métodos para calcular el promedio de tiempo de ejecución. Si se quieren compilar todos, agregar compile al final de la orden.*
