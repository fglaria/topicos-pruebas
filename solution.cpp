#include "solution.h"

namespace solution
{
    // Parallel evaluation of neighbors
    void evaluateNeighbors(const Vec16uc& possible, const Vec16uc& searched,
        uint32_t* currentX, uint32_t* comparedX,
        std::map<uint32_t, std::set<uint32_t>>& allNeighbors)
    {
        // std::cout << "evaluateNeighbors" << std::endl;
        // Parallel AND
        const Vec16uc masksResult = possible & searched;

        // Size of vectors
        const uint8_t maxSize = sizeof(masksResult);

        // std::cout << "Possible: ";
        // for (int i = 0; i < 16; ++i)
        // {
        //     std::cout << binary::print(possible[i], 8) << " ";
        // }
        // std::cout << std::endl;

        // std::cout << "Searched: ";
        // for (int i = 0; i < 16; ++i)
        // {
        //     std::cout << binary::print(searched[i], 8) << " ";
        // }
        // std::cout << std::endl;

        // std::cout << "Result:   ";
        // for (uint8_t i = 0; i < maxSize; ++i)
        // {
        //     std::cout << binary::print(masksResult[i], 8) << " ";
        // }
        // std::cout << std::endl;

        // std::cout << "value X:  ";
        // for (uint8_t i = 0; i < maxSize; i++) {
        //     std::cout << std::setfill(' ') << std::setw(8) << (uint32_t) comparedX[i] << " ";
        // }
        // std::cout << std::endl;
        // std::cout << std::endl;

        // Create a boolean vector of comparisons results
        const Vec16cb isNeighbor = masksResult != 0;

        // For each boolean value, write a bit in a short (reversed)
        unsigned short isNeighborShort = to_bits(isNeighbor);
        // std::cout << "isNeighborShort " << binary::print(isNeighborShort, 16) << std::endl;

        // If is not zero, some neighbor was found
        if (0 != isNeighborShort)
        {
            // For every bit in inNeighbor, let's find wich one
            // It's reversed because isNeighbor is reversed
            for (unsigned char i = 0; i < maxSize; ++i)
            {
                // Get LSB
                const bool bitValue = isNeighborShort & 0x01;

                // Remove LSB
                isNeighborShort >>= 1;

                // Get Possible X
                const unsigned int xC = currentX[i];
                const unsigned int xN = comparedX[i];

                // If bit is one, is neighbor. Else, no neighbor.
                if(bitValue)
                {
                    // Insert neighbor into set
                    allNeighbors[xC].insert(xN);
                    allNeighbors[xN].insert(xC);
                }
            }
        }

        return;
    }



    // read compressed data and returns pointer to B4
    unsigned char * readData(const std::string path, sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1, sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt)
    {
        // Paths to SDSL & binary files
        std::string sXbin = path+".seqX.bin-wm_int.sdsl";
        std::string sB1bin = path+".B-rrr-64.sdsl";
        std::string sB4bin = path+".B4.bin";
        std::string sY3bin = path+".seqY3.bin-wm_int.sdsl";

        // Open B4 file
        std::ifstream b4file (sB4bin, std::ios::in | std::ios::binary | std::ios::ate);

        // Reading files
        // Load X, B1
        load_from_file(x_wt, sXbin.c_str());
        load_from_file(rrrB1, sB1bin.c_str());
        // Construct Y3
        load_from_file(y3_wt, sY3bin.c_str());

        // Read B4
        std::streampos size = b4file.tellg();
        unsigned char * B4 = new unsigned char [size];
        b4file.seekg (0, std::ios::beg);
        b4file.read ((char*)B4, size);
        b4file.close();

        return B4;
    }


    // read compressed data
    void readDataB4(const std::string path, sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm,
        sdsl::rrr_vector<63> &rrrB1, sdsl::wt_hutu<sdsl::rrr_vector<63>> &b4_wt,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm)
    {
        // Paths to SDSL & binary files
        std::string sXbin = path+".seqX.bin-wm_int.sdsl";
        std::string sB1bin = path+".B-rrr-64.sdsl";
        std::string sB4bin = path+".B4.bin-wt_hutu.sdsl";
        std::string sY3bin = path+".seqY3.bin-wm_int.sdsl";

        // // Open B4 file
        // std::ifstream b4file (sB4bin, std::ios::in | std::ios::binary | std::ios::ate);

        // Reading files
        // Load X, B1, B4, Y3
        load_from_file(x_wm, sXbin.c_str());
        load_from_file(rrrB1, sB1bin.c_str());

        load_from_file(b4_wt, sB4bin.c_str());
        load_from_file(y3_wm, sY3bin.c_str());
    }



    std::string time_representation(double t) {
        std::string units = " [ms]";

        if (1000 < t)
        {
            t /= 1000;
            units = " [s]";
        }

        return std::to_string(t) + units;
    }

}
