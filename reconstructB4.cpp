// g++ -std=c++11 reconstructB4.cpp -o reconstructB4 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <iostream>
#include <fstream>
#include <string>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

int main(int argc, char const *argv[])
{
    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    const std::string sB4bin = path+".B4.bin";
    const std::string sB4binWM = path+".B4.bin.wm_int";
    const std::string sB4binWT = path+".B4.bin.wt_blcd";
    const std::string sB4binWThuff = path+".B4.bin.wt_huff";
    const std::string sB4binWThutu = path+".B4.bin.wt_hutu";



    // Read binary
    std::ifstream b4file (sB4bin, std::ios::in | std::ios::binary | std::ios::ate);
    std::streampos size = b4file.tellg();
    unsigned char * B4 = new unsigned char [size];
    b4file.seekg (0, std::ios::beg);
    b4file.read ((char*)B4, size);
    b4file.close();

    std::ofstream outBin(path + ".recon.bin", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < size; ++i)
    {
        outBin << (int) B4[i] << " ";
    }
    outBin.close();

    std::cout << "Bin size " << size << std::endl;


    // Read wm_int
    sdsl::wm_int<sdsl::rrr_vector<63>> b4_wm;
    load_from_file(b4_wm, sB4binWM.c_str());

    std::ofstream outWM(path + ".recon.wm_int", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < b4_wm.size(); ++i)
    {
        outWM << b4_wm[i] << " ";
    }
    outWM.close();

    std::cout << "wm_int size " << b4_wm.size() << std::endl;



    // Read wt_blcd
    sdsl::wt_blcd<sdsl::rrr_vector<63>> b4_wt;
    load_from_file(b4_wt, sB4binWT.c_str());

    std::ofstream outWT(path + ".recon.wt_blcd", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < b4_wt.size(); ++i)
    {
        outWT << (int) b4_wt[i] << " ";
    }
    outWT.close();

    std::cout << "wt_blcd size " << b4_wt.size() << std::endl;



    // Read wt_huff
    sdsl::wt_huff<sdsl::rrr_vector<63>> b4_wthuff;
    load_from_file(b4_wthuff, sB4binWThuff.c_str());

    std::ofstream outWThuff(path + ".recon.wthuff", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < b4_wthuff.size(); ++i)
    {
        outWThuff << (int) b4_wthuff[i] << " ";
    }
    outWThuff.close();

    std::cout << "wt_huff size " << b4_wthuff.size() << std::endl;



    // Read wt_hutu
    sdsl::wt_hutu<sdsl::rrr_vector<63>> b4_wthutu;
    load_from_file(b4_wthutu, sB4binWThutu.c_str());

    std::ofstream outWThutu(path + ".recon.wthutu", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < b4_wthutu.size(); ++i)
    {
        outWThutu << (int) b4_wthutu[i] << " ";
    }
    outWThutu.close();

    std::cout << "wt_hutu size " << b4_wthutu.size() << std::endl;


    return 0;
}